package br.edu.iftm.views;

import java.util.ArrayList;

import br.edu.iftm.controllers.Controlador;
import br.edu.iftm.models.Campo;
import br.edu.iftm.models.ConteudoConsulta;
import br.edu.iftm.models.Tabela;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class JanelaTabela {
	private String nomeBanco, nomeTabela;
	private VBox vboxTable;
	private TabPane painelJanelaTable;
	private Controlador controlador;
	private ArrayList<CampoTabela> camposTabela;
	private Tab abaEstrutura, abaConteudo, abaParser;
	private VBox vboxAbaEstrutura, vboxAbaConteudo, vboxAbaParser;
	private TreeView<String> treeView;
	private Button bExcluirTabela;
	@SuppressWarnings("rawtypes")
	private TableView tabelaConsulta;
	
	public JanelaTabela(Controlador controlador, TreeView<String> treeView)
	{
		this.controlador = controlador;
		camposTabela = new ArrayList<CampoTabela>();
		this.treeView = treeView;
		carregarTabPaneJanelaTabela();
	}
	
	public void carregarTabPaneJanelaTabela()
	{
		painelJanelaTable = new TabPane();
		painelJanelaTable.setMinSize(900, 600);
		vboxTable = new VBox();	
		abaEstrutura= new Tab("Estrutura");
		vboxAbaEstrutura = new VBox(15);
		abaEstrutura.setClosable(false);
		abaEstrutura.setContent(vboxAbaEstrutura);
		abaConteudo = new Tab("Conteudo");
		vboxAbaConteudo = new VBox(15);
		abaConteudo.setContent(vboxAbaConteudo);
		abaConteudo.setClosable(false);
		abaParser = new Tab("SQL");
		abaParser.setClosable(false);
		vboxAbaParser = new VBox();
		abaParser.setContent(vboxAbaParser);
		
		painelJanelaTable.getTabs().addAll(abaConteudo, abaEstrutura, abaParser);
	}
	
	private void carregarAbaEstrutura()
	{
		vboxAbaEstrutura.getChildren().clear();
		
		int idBanco = controlador.buscarIdBanco(nomeBanco);
		int idTabela = controlador.buscarIdTabela(idBanco, nomeTabela);
		Tabela tabela = controlador.getBancos().get(idBanco).getTabelas().get(idTabela);		
		vboxAbaEstrutura.getChildren().add(JanelaNovaTabela.criarLabels());
		vboxAbaEstrutura.getChildren().add(new Separator(Orientation.HORIZONTAL));
		for(Campo campo : tabela.getCampos())
		{
			String nomeCampo = campo.getNome();
			String tipo = campo.getTipo().toString().toUpperCase();
			String tamanho = ""+campo.getTipo().getTamanho();
			String atributos = (campo.getRestricoes().isSigned() ? "SIGNED" : "UNSIGNED");
			boolean isNulo = campo.getRestricoes().isNulo();		
			String defaultValue = campo.getRestricoes().getDefaultValue();
			System.out.println("Campo default value: " + defaultValue);
			String indice;
			if(campo.getRestricoes().isPrimary())
				indice = "PRIMARY";
			else if(campo.getRestricoes().isUnique())
				indice = "UNIQUE";
			else
				indice = "";
			boolean isAutoincrement = campo.getRestricoes().isAutoIncrement();
			
			CampoTabela novoCampo = new CampoTabela();
			novoCampo.criarTxtNome(nomeCampo);
			novoCampo.criarCbTipo(tipo);
			novoCampo.criarTxtTamanho(tamanho);
			novoCampo.criarCbAtributos(atributos);
			novoCampo.criarCbIsNulo(isNulo);
			novoCampo.criarTxtDefaultValue((defaultValue == null)? "": defaultValue);
			novoCampo.criarCbIndice(indice);
			novoCampo.criarCbIsAutoIncrement(isAutoincrement);
			
			vboxAbaEstrutura.getChildren().add(novoCampo.gerarHboxNovoCampo());
			// atributos(Signed/unsigned), isNulo, DefaultValue, Indice(Primary/unique), AI
			System.out.println(campo.getNome());
			
		}
	}
	
	@SuppressWarnings({"rawtypes" })
	private void carregarAbaConteudo()
	{
		vboxAbaConteudo.getChildren().clear();
		int idBanco = controlador.buscarIdBanco(nomeBanco);
		int idTabela = controlador.buscarIdTabela(idBanco, nomeTabela);
		String[][] dadosMatriz = controlador.getMatrizRegistros(idBanco, idTabela);
		String[] vetorCampos = controlador.getVetorCampos(idBanco, idTabela);
		TableView tabela = TabelaDinamica.gerarTabela(vetorCampos, dadosMatriz, controlador.getQtdRegistros(idBanco, idTabela));
		vboxAbaConteudo.getChildren().add(tabela);
		abaConteudo.setClosable(false);
	}
	
	private void carregarAbaParser()
	{
		vboxAbaParser.getChildren().clear();
		TextArea txtSqlParser = new TextArea("");
		txtSqlParser.setMinSize(550, 150);
		txtSqlParser.setMaxSize(550, 150);
		Button btExecutar = new Button("Executar");
		btExecutar.setOnAction(e->executarQuery(txtSqlParser.getText()));
		btExecutar.setMinSize(70, 40);
		HBox hbQuery = new HBox(5);
		hbQuery.setAlignment(Pos.CENTER);
		hbQuery.getChildren().addAll(txtSqlParser, btExecutar);
		vboxAbaParser.setPadding(new Insets(0, 0, 0, 5));
		tabelaConsulta = JanelaParserSql.criarTabelaTeste(885, 400);
		vboxAbaParser.getChildren().add(hbQuery);
		vboxAbaParser.getChildren().add(tabelaConsulta);
		
	}	
	
	private void executarQuery(String query)
	{
		System.out.println("Executou query!");
		ConteudoConsulta cc = controlador.executarQuery(query, nomeBanco);
		if(cc != null)
		{
			vboxAbaParser.getChildren().remove(tabelaConsulta);
			tabelaConsulta = TabelaDinamica.gerarTabela(cc, 885, 400);
			vboxAbaParser.getChildren().add(tabelaConsulta);
		}
	}
	
	
	public void atualizar(String nomeBanco, String nomeTabela)
	{
		this.nomeBanco = nomeBanco;
		this.nomeTabela = nomeTabela;
		carregarAbaEstrutura();
		carregarAbaConteudo();
		carregarAbaParser();
	}

	public TabPane getPainelJanelaTable() {
		return painelJanelaTable;
	}

	public void setPainelJanelaTable(TabPane painelJanelaTable) {
		this.painelJanelaTable = painelJanelaTable;
	}
	
	
	
}
