package br.edu.iftm.views;

import br.edu.iftm.models.ConteudoConsulta;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.util.Callback;

public class TabelaDinamica {
	
	public static TableView gerarTabela(String[] nomeColunas, String[][] dadosMatriz, int qtdRegistros)
	{
		return gerarTabela(nomeColunas, dadosMatriz, qtdRegistros, 897, 570);
	}
	
	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	public static TableView gerarTabela(ConteudoConsulta cc, int tabTamX, int tabTamY)
	{
		return gerarTabela(cc.getNomeColunas(), cc.getValores(), cc.getIndiceValor()/*cc.getQtdValores()*/, tabTamX, tabTamY);
	}
	
	@SuppressWarnings({ "rawtypes", "unused", "unchecked" })
	public static TableView gerarTabela(String[] nomeColunas, String[][] dadosMatriz, int qtdRegistros, int tabTamX, int tabTamY)
	{
		TableView tabela = new TableView();
		//double tabTamX = 897, tabTamY = 570;
		tabela.setMinSize(tabTamX, tabTamY);
		tabela.setMaxSize(tabTamX, tabTamY);
		
		// Carregar colunas
		for(int i=0; i < nomeColunas.length; i++)
		{
			tabela.getColumns().addAll(gerarColuna(nomeColunas[i], i, nomeColunas.length, tabTamX));
		}
		
		// Carregar valores
		ObservableList<ObservableList> dados = gerarDadosTabela(dadosMatriz, qtdRegistros, nomeColunas.length);
		
		tabela.setItems(dados);
		
		
		return tabela;
	}
	
	@SuppressWarnings("rawtypes")
	private static ObservableList<ObservableList> gerarDadosTabela(String[][] dadosMatriz, int qtdRegistros, int qtdCampos)
	{
		ObservableList<ObservableList> data = FXCollections.observableArrayList();
		
		for(int i=0; i < qtdRegistros; i++)
		{
			ObservableList<String> row = FXCollections.observableArrayList();
			for(int j=0; j < qtdCampos; j++)
			{
				row.add(dadosMatriz[i][j]);
			}
			/*boolean deuCerto = true;
			for(int j=0; j < row.size(); j++)
			{
				if(row.get(j).equals(null))
					deuCerto = false;
			}
			
			if(deuCerto)*/
			data.add(row);
		}
		
		return data;
	}
	
	@SuppressWarnings("rawtypes")
	private static TableColumn<ObservableList, String> gerarColuna(String nomeColuna, int indiceColuna, int qtdColunas, double tabTamX)
	{
		TableColumn<ObservableList, String> coluna = new TableColumn<ObservableList, String>(nomeColuna);
		coluna.setMinWidth(tabTamX / qtdColunas);
		coluna.setResizable(true);
		coluna.setSortable(false);
		coluna.setEditable(false);
		
		coluna.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList,String>, ObservableValue<String>>() {
			
			@Override
			public ObservableValue<String> call(CellDataFeatures<ObservableList, String> param) {
				return new SimpleStringProperty(param.getValue().get(indiceColuna).toString());
			}
		});
		return coluna;
	}
}
