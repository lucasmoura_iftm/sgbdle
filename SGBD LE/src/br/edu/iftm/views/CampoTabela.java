package br.edu.iftm.views;

import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class CampoTabela {
	private HBox hbCampo;
	private TextField txtNome, txtDefaultValue;
	private ComboBox<String> cbTipo, cbAtributos, cbIndice;
	private TextField txtTamanho;
	private CheckBox cbIsNulo, cbIsAutoIncrement;
	
	public CampoTabela()
	{
		
	}
	
	public void criarTxtNome(String nome)
	{
		txtNome = new TextField(nome);
	}
	
	public void criarCbTipo(String value)
	{
		cbTipo = new ComboBox<String>(); 
		cbTipo.getItems().addAll("INT", "VARCHAR", "CHAR", "TEXT", "DATE", "DATETIME", "FLOAT");
		cbTipo.setValue(value);
	}
	
	public void criarTxtTamanho(String tamanho)
	{
		txtTamanho = new TextField(tamanho);
		txtTamanho.setMaxWidth(50);
	}
	
	public void criarCbAtributos(String value)
	{
		cbAtributos = new ComboBox<String>();
		cbAtributos.getItems().addAll("", "SIGNED", "UNSIGNED");
		cbAtributos.setValue(value);
	}
	
	public void criarTxtDefaultValue(String value)
	{
		if(value.equals("null"))
			txtDefaultValue = new TextField();
		else
			txtDefaultValue = new TextField(value);
		txtDefaultValue.setPromptText("Default value");
	}
	
	public void criarCbIsNulo(boolean selected)
	{
		 cbIsNulo = new CheckBox();
		 cbIsNulo.setSelected(selected);
	}
	
	public void criarCbIndice(String value)
	{
		cbIndice = new ComboBox<String>();
		cbIndice.getItems().addAll("", "PRIMARY", "UNIQUE");
		cbIndice.setValue(value);
	}
	
	public HBox gerarHboxNovoCampo()
	{
		HBox hbNovoCampo = new HBox(15);
		hbNovoCampo.getChildren().addAll(txtNome, cbTipo, txtTamanho, cbAtributos, cbIsNulo, txtDefaultValue, cbIndice, cbIsAutoIncrement);
		return hbNovoCampo;
	}
	
	public void criarCbIsAutoIncrement(boolean selected)
	{
		cbIsAutoIncrement = new CheckBox();
		cbIsAutoIncrement.setSelected(selected);
	}
	
	public String getNome()
	{
		return txtNome.getText();
	}
	
	public String getTipo()
	{
		return cbTipo.getValue().toUpperCase();
	}
	
	public int getTamanho()
	{
		if(txtTamanho.getText().isEmpty())
			return 0;
		else
			return Integer.parseInt(txtTamanho.getText());
	}
	
	public String getAtributo()
	{
		return cbAtributos.getValue().toUpperCase();
	}
	
	public String getDefaultValue()
	{
		return txtDefaultValue.getText();
	}
	
	public boolean isNulo()
	{
		return cbIsNulo.isSelected();
	}
	
	public String getIndice()
	{
		return cbIndice.getValue().toUpperCase();
	}
	
	public boolean isAutoIncrement()
	{
		return cbIsAutoIncrement.isSelected();
	}
	
	
	public boolean estaVazio()
	{
		return txtNome.getText().isEmpty();
	}
	

	public HBox getHbCampo() {
		return hbCampo;
	}

	public void setHbCampo(HBox hbCampo) {
		this.hbCampo = hbCampo;
	}



	public TextField getTxtNome() {
		return txtNome;
	}

	public void setTxtNome(TextField txtNome) {
		this.txtNome = txtNome;
	}

	public TextField getTxtDefaultValue() {
		return txtDefaultValue;
	}

	public void setTxtDefaultValue(TextField txtDefaultValue) {
		this.txtDefaultValue = txtDefaultValue;
	}

	public ComboBox<String> getCbTipo() {
		return cbTipo;
	}

	public void setCbTipo(ComboBox<String> cbTipo) {
		this.cbTipo = cbTipo;
	}

	public ComboBox<String> getCbAtributos() {
		return cbAtributos;
	}

	public void setCbAtributos(ComboBox<String> cbAtributos) {
		this.cbAtributos = cbAtributos;
	}

	public ComboBox<String> getCbIndice() {
		return cbIndice;
	}

	public void setCbIndice(ComboBox<String> cbIndice) {
		this.cbIndice = cbIndice;
	}

	public TextField getTxtTamanho() {
		return txtTamanho;
	}

	public void setTxtTamanho(TextField txtTamanho) {
		this.txtTamanho = txtTamanho;
	}

	public CheckBox getCbIsNulo() {
		return cbIsNulo;
	}

	public void setCbIsNulo(CheckBox cbIsNulo) {
		this.cbIsNulo = cbIsNulo;
	}

	public CheckBox getCbIsAutoIncrement() {
		return cbIsAutoIncrement;
	}

	public void setCbIsAutoIncrement(CheckBox cbIsAutoIncrement) {
		this.cbIsAutoIncrement = cbIsAutoIncrement;
	}

	
}
