package br.edu.iftm.views;

import br.edu.iftm.controllers.Controlador;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class JanelaParserSql extends Scene{
	private VBox vboxJanelaParser;
	private Controlador controlador;
	private TextArea txtSqlParser;
	
	public JanelaParserSql(VBox vboxJanelaParser, Controlador controlador)
	{
		super(vboxJanelaParser, 800, 600);
		this.vboxJanelaParser = vboxJanelaParser;
		this.controlador = controlador;
		iniciarInterface();
	}
	
	private void iniciarInterface()
	{
		txtSqlParser = new TextArea("");
		txtSqlParser.setMinSize(550, 150);
		txtSqlParser.setMaxSize(550, 150);
		Button btExecutar = new Button("Executar");
		btExecutar.setOnAction(e->executarQuery());
		btExecutar.setMinSize(70, 40);
		HBox hbQuery = new HBox(5);
		hbQuery.setAlignment(Pos.CENTER);
		hbQuery.getChildren().addAll(txtSqlParser, btExecutar);
		vboxJanelaParser.setPadding(new Insets(0, 0, 0, 5));
		
		vboxJanelaParser.getChildren().add(hbQuery);
		vboxJanelaParser.getChildren().add(criarTabelaTeste());
	}
	
	private void executarQuery()
	{
		
	}
	
	public static TableView criarTabelaTeste()
	{
		String[][] dadosMatriz = {{"Lucas", "21", "Masculino"}, {"Vanessa", "24", "Feminino"}, {"Joao", "40", "Masculino"}};
		String[] nomeColunas = {"Nome", "Idade", "Sexo"};
		
		return TabelaDinamica.gerarTabela(nomeColunas, dadosMatriz, 3);
	}
	
	public static TableView criarTabelaTeste(int tamX, int tamY)
	{
		String[][] dadosMatriz = {{"Lucas", "21", "Masculino"}, {"Vanessa", "24", "Feminino"}, {"Joao", "40", "Masculino"}};
		String[] nomeColunas = {"Nome", "Idade", "Sexo"};
		
		return TabelaDinamica.gerarTabela(nomeColunas, dadosMatriz, 3, tamX, tamY);
	}
}
