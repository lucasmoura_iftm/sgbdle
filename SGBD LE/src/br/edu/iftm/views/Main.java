package br.edu.iftm.views;

import java.util.ArrayList;

import br.edu.iftm.models.Banco;
import br.edu.iftm.models.Campo;
import br.edu.iftm.models.Tabela;
import br.edu.iftm.models.tipos.TipoChar;
import br.edu.iftm.models.tipos.TipoDate;
import br.edu.iftm.models.tipos.TipoDateTime;
import br.edu.iftm.models.tipos.TipoFloat;
import br.edu.iftm.models.tipos.TipoInt;
import br.edu.iftm.models.tipos.TipoText;
import br.edu.iftm.models.tipos.TipoVarChar;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main extends Application{
	public static void criarBancoTeste()
	{
		Banco carro = new Banco("Carro");
		Tabela tabela = new Tabela("produto", carro.getNome());
		Campo campoId = new Campo("id",new TipoInt(), tabela.getBanco(), tabela.getNome(), tabela.getCampos().size());
		campoId.getRestricoes().setAutoIncrement(false);
		campoId.getRestricoes().setPrimary(true);
		tabela.getCampos().add(campoId);
		tabela.getCampos().add(new Campo("nome", new TipoVarChar(10), tabela.getBanco(), tabela.getNome(), tabela.getCampos().size()-1));
		tabela.getCampos().add(new Campo("descricao", new TipoText(), tabela.getBanco(), tabela.getNome(), tabela.getCampos().size()-1));
		tabela.getCampos().add(new Campo("etiqueta", new TipoChar(30), tabela.getBanco(), tabela.getNome(), tabela.getCampos().size()-1));
		tabela.getCampos().add(new Campo("validade", new TipoDate(), tabela.getBanco(), tabela.getNome(), tabela.getCampos().size()-1));
		tabela.getCampos().add(new Campo("fabricacao", new TipoDateTime(), tabela.getBanco(), tabela.getNome(), tabela.getCampos().size()-1));
		tabela.getCampos().add(new Campo("preco-sugerido", new TipoFloat(), tabela.getBanco(), tabela.getNome(), tabela.getCampos().size()-1));
		tabela.criarTabela();
	}
	
	public static void adicionarRegistroTeste(String nomeBanco, String nomeTabela)
	{
		
		ArrayList<Banco> bancos = Banco.carregarBancos();
		int idBanco = Banco.getIdBanco(bancos, nomeBanco);
		int idTabela = Tabela.getIdTabela(bancos.get(idBanco).getTabelas(), nomeTabela);
		Tabela tabela = bancos.get(0).getTabelas().get(0);
		
		// (id, nome, descricao)
		// (nome, descricao
		ArrayList<String> colunas = new ArrayList<String>();
		
		colunas.add("nome");
		colunas.add("descricao");
		colunas.add("etiqueta");
		colunas.add("validade");
		colunas.add("fabricacao");
		colunas.add("preco-sugerido");
		colunas.add("id");
		
		ArrayList<String> conteudoRegistro = new ArrayList<String>();
		conteudoRegistro.add("Leite");
		conteudoRegistro.add("Descricao do leite");
		conteudoRegistro.add("Leite para criancas");
		conteudoRegistro.add("2016-10-01");
		conteudoRegistro.add("2015-12-20 20:02:35");
		conteudoRegistro.add("5.00");
		conteudoRegistro.add("9");
		
		tabela.addRegistro(colunas, conteudoRegistro);
	}

	public static void main(String[] args) {
		//criarBancoTeste();
		//adicionarRegistroTeste("Carro", "produto");
		launch();
	}

	@Override
	public void start(Stage palco) throws Exception {
		
		JanelaPrincipal janelaPrincipal = new JanelaPrincipal(new GridPane());
		palco.setScene(janelaPrincipal);
		palco.setTitle("SGBD L.E. - v0.1");
		palco.show();
		palco.setOnCloseRequest(new EventHandler<WindowEvent>() {
			
			@Override
			public void handle(WindowEvent event) {
				finalizarAplicacao(palco);
			}
		});
	}
	
	public void finalizarAplicacao(Stage palco)
	{
		palco.close();
		Platform.exit();
	}

}
