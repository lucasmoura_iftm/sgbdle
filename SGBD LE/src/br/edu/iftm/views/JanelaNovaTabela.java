package br.edu.iftm.views;

import java.util.ArrayList;

import br.edu.iftm.controllers.Controlador;
import br.edu.iftm.models.Campo;
import br.edu.iftm.models.Restricoes;
import br.edu.iftm.models.Tabela;
import br.edu.iftm.models.tipos.Tipo;
import br.edu.iftm.models.tipos.TipoChar;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class JanelaNovaTabela {
	private VBox vboxNewTable, vboxCampos;
	private TextField txtNomeTb, txtQtdColunas;
	private Button btAddColunas, btDelColunas, btSalvar, btLimpar, bExcluirTabela, bAtualizarTabela;
	private ScrollPane spCampos;
	private ArrayList<CampoTabela> camposTabela;
	private int indiceCampo = 0;
	private Controlador controlador;
	private JanelaPrincipal janelaPrincipal;
	private GridPane grid;
	
	public JanelaNovaTabela(Controlador controlador, JanelaPrincipal janelaPrincipal, GridPane grid)
	{
		this.controlador = controlador;
		this.janelaPrincipal = janelaPrincipal;
		this.grid = grid;
		camposTabela = new ArrayList<CampoTabela>();
		carregarVBoxNewTable();
	}
	
	private void carregarVBoxNewTable()
	{
		vboxCampos = new VBox(10);
		vboxNewTable = new VBox(10);
		HBox hbCabecalho = criarCabecalho();
		HBox hbLabels = criarLabels();
		Separator separatorCab= new Separator(Orientation.HORIZONTAL);
		HBox hbRodape = criarRodape();
		Separator separatorRod= new Separator(Orientation.HORIZONTAL);
			
		spCampos = new ScrollPane();
		spCampos.setMinWidth(820);
		spCampos.setMaxHeight(290);
		spCampos.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
		
		vboxNewTable.getChildren().addAll(hbCabecalho, hbLabels, separatorCab, spCampos, separatorRod, hbRodape);
		criarCampos(8);
	}
	
	
	private HBox criarCabecalho()
	{
		HBox hbCabecalho = new HBox(5);
		Label lbNomeTab = new Label("Nome da tabela: ");
		txtNomeTb = new TextField();
		
		txtQtdColunas = new TextField("1");
		txtQtdColunas.setMaxWidth(40);
		
		btAddColunas = new Button("Adicionar");
		btAddColunas.setOnAction(e->criarCampos(1));
		btDelColunas = new Button("Remover");
		btDelColunas.setOnAction(e->removerCampos(1));
		
		
		hbCabecalho.getChildren().addAll(lbNomeTab, txtNomeTb, txtQtdColunas, btAddColunas, btDelColunas);
		return hbCabecalho;
	}
	
	private HBox criarRodape()
	{
		HBox hbRodape = new HBox(10);
		hbRodape.setAlignment(Pos.CENTER);
		btSalvar = new Button("Salvar");
		btSalvar.setOnAction(e->criarTabela());
		btLimpar = new Button("Limpar");
		btLimpar.setOnAction(e->limparTabela());
		hbRodape.getChildren().addAll(btSalvar, btLimpar);
		return hbRodape;
	}
	
	private void criarTabela()
	{
		System.out.println("Criar tabela");
		
		ArrayList<Campo> campos = new ArrayList<Campo>();;
		Restricoes restricoes;
		
		for(int i=0; i < camposTabela.size(); i++)
		{
			if(!camposTabela.get(i).estaVazio())
			{
				String nomeCampo = camposTabela.get(i).getNome();
				String tipo = camposTabela.get(i).getTipo();
				int tamanho = camposTabela.get(i).getTamanho();
				String atributo = camposTabela.get(i).getAtributo();
				String defaultValue = camposTabela.get(i).getDefaultValue();
				boolean isNulo = camposTabela.get(i).isNulo();
				String indice = camposTabela.get(i).getIndice();
				boolean autoIncrement = camposTabela.get(i).isAutoIncrement();
				
				boolean isPrimary = (indice == "PRIMARY")? true : false ;
				boolean isUnico   = (indice == "UNIQUE") ? true : false ;
				
				restricoes = new Restricoes(isPrimary, autoIncrement, isUnico, "Carro",
											txtNomeTb.getText(), camposTabela.size());
				restricoes.setDefaultValue(defaultValue);
				restricoes.setNulo(isNulo);
				
				campos.add(new Campo(nomeCampo, Tipo.retornaTipo(tipo, tamanho),restricoes));
				
				System.out.println(nomeCampo+";"+tipo+";"+tamanho+";"+atributo+";"+isNulo+";"+defaultValue+";"+indice+";"+autoIncrement);
			}
		}
		Tabela tabela = new Tabela(txtNomeTb.getText(), "Carro", campos);
		if(tabela.criarTabela())
		{
			controlador.getBancos().get(0).addTabela(tabela);
			janelaPrincipal.getTreeView().getSelectionModel().getSelectedItem().getParent().getChildren().add(
					new TreeItem(txtNomeTb.getText(), new ImageView(new Image("file:imagens//table.jpg", 20, 20, true, false))));
			//grid.getChildren().remove(0);
			//grid.getChildren().add(janelaPrincipal.carregarTreeView());
		}
	}
	
	private void limparTabela()
	{
		for (CampoTabela campoTabela : camposTabela) 
		{
			campoTabela.getTxtNome().setText("");
			campoTabela.getCbTipo().setValue("INT");
			campoTabela.getTxtTamanho().setText("");
			campoTabela.getCbAtributos().setValue("");
			campoTabela.getTxtDefaultValue().setText("");
			campoTabela.getCbIsNulo().setSelected(false);
			campoTabela.getCbIndice().setValue("");
			campoTabela.getCbIsAutoIncrement().setSelected(false);
		}
	}
	
	private void criarCampos(int qtd)
	{
		for(int i=0; i < qtd; i++)
		{
			vboxCampos.getChildren().add(gerarCampo());
		}
		spCampos.setContent(vboxCampos);
	}
	
	private void removerCampos(int qtd)
	{
		for(int i=0; i < qtd; i++)
		{
			if(camposTabela.size() > 1)
			{
				vboxCampos.getChildren().remove(camposTabela.get(camposTabela.size()-1).getHbCampo());
				camposTabela.remove(camposTabela.size()-1);
			}
			System.out.println("Campos restantes: " + vboxNewTable.getChildren().size() + "; " + camposTabela.size());
		}
		spCampos.setContent(vboxCampos);
	}
	
	private HBox gerarCampo()
	{
		HBox hbNovoCampo = new HBox(15);
		CampoTabela novoCampoTabela = new CampoTabela();
		novoCampoTabela.setHbCampo(hbNovoCampo);
		
		TextField txtNomeCampo1 = new TextField();
		txtNomeCampo1.setPromptText("Nome");
		novoCampoTabela.setTxtNome(txtNomeCampo1);

		ComboBox<String> cbTipo = new ComboBox<String>(); 
		cbTipo.getItems().addAll("INT", "VARCHAR", "CHAR", "TEXT", "DATE", "DATETIME", "FLOAT");
		cbTipo.setValue("INT");
		novoCampoTabela.setCbTipo(cbTipo);
		
		TextField txtTamanhoCampo = new TextField();
		txtTamanhoCampo.setMaxWidth(50);
		novoCampoTabela.setTxtTamanho(txtTamanhoCampo);
		
		ComboBox<String> cbAtributos = new ComboBox<String>();
		cbAtributos.getItems().addAll("", "SIGNED", "UNSIGNED");
		cbAtributos.setValue("");
		novoCampoTabela.setCbAtributos(cbAtributos);
		
		TextField txtDefaultValue = new TextField();
		txtDefaultValue.setPromptText("Default value");
		novoCampoTabela.setTxtDefaultValue(txtDefaultValue);
		
		CheckBox cbIsNulo = new CheckBox();
		novoCampoTabela.setCbIsNulo(cbIsNulo);
		
		ComboBox<String> cbIndice = new ComboBox<String>();
		cbIndice.getItems().addAll("", "PRIMARY", "UNIQUE");
		cbIndice.setValue("");
		novoCampoTabela.setCbIndice(cbIndice);
		
		CheckBox cbIsAutoIncrement = new CheckBox();
		novoCampoTabela.setCbIsAutoIncrement(cbIsAutoIncrement);
		
		camposTabela.add(novoCampoTabela);
		hbNovoCampo.getChildren().addAll(txtNomeCampo1, cbTipo, txtTamanhoCampo, cbAtributos, cbIsNulo, txtDefaultValue, cbIndice, cbIsAutoIncrement);
		return hbNovoCampo;
	}
	
	public static HBox criarLabels()
	{
		HBox hbLabels = new HBox(0);
		Label lbNome = new Label("Nome");
		Label lbTipo = new Label("Tipo");
		lbTipo.setPadding(new Insets(0, 0, 0, 130));
		Label lbTamanho = new Label("Tamanho");
		lbTamanho.setPadding(new Insets(0, 0, 0, 90));
		Label lbAtributos = new Label("Atributos");
		lbAtributos.setPadding(new Insets(0, 0, 0, 40));
		Label lbNulo = new Label("Nulo");
		lbNulo.setPadding(new Insets(0, 0, 0, 45));
		Label lbDefaultValue = new Label("Default Value");
		lbDefaultValue.setPadding(new Insets(0, 0, 0, 50));
		Label lbIndice = new Label("Indice");
		lbIndice.setPadding(new Insets(0, 0, 0, 80));
		Label lbAutoIncrement = new Label("A.I.");
		lbAutoIncrement.setPadding(new Insets(0, 0, 0, 55));
		hbLabels.getChildren().addAll(lbNome, lbTipo, lbTamanho, lbAtributos, lbNulo, lbDefaultValue, lbIndice, lbAutoIncrement);
		return hbLabels;
	}

	public VBox getVboxNewTable() {
		return vboxNewTable;
	}

	public void setVboxNewTable(VBox vboxNewTable) {
		this.vboxNewTable = vboxNewTable;
	}
	
	
}