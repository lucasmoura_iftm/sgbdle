package br.edu.iftm.views;

import java.util.ArrayList;

import br.edu.iftm.controllers.Controlador;
import br.edu.iftm.models.Banco;
import br.edu.iftm.models.Tabela;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

public class JanelaPrincipal extends Scene{
	private VBox raiz;
	private GridPane grid;
	private Controlador controlador;
	private TextArea txtSqlParser;
	private VBox vboxConteudoParser, vboxNewTable, vboxGrid;
	private TabPane painelJanelaTable;
	private JanelaNovaTabela janelaNewTab;
	private JanelaTabela janelaTabela;
	private String bdSelecionado;
	private TreeView<String> treeView;
	private Button bExcluirTabela, bAtualizarNomeTabela;
	private TextField txtNomeTabela;
	
	public JanelaPrincipal(GridPane grid) {
		super(grid, 1050, 600);
		this.grid = grid;
		controlador = new Controlador();
		treeView = carregarTreeView();
		janelaNewTab = new JanelaNovaTabela(controlador, this, grid);
		janelaTabela = new JanelaTabela(controlador, treeView);
		vboxNewTable = janelaNewTab.getVboxNewTable();
		painelJanelaTable = janelaTabela.getPainelJanelaTable();
		carregarInterface();
	}
	
	private void carregarInterface()
	{
		grid.setHgap(2);
		grid.setVgap(1);
		
		carregarVBoxConteudoParser();
		//carregarVBoxNewTable();	
		
		grid.add(treeView, 0, 0);
		grid.add(vboxConteudoParser/*vboxNewTable*/, 1, 0);

	}
	
	private void carregarVBoxConteudoParser()
	{
		vboxConteudoParser = new VBox(10);
		txtSqlParser = new TextArea("");
		txtSqlParser.setMinSize(550, 150);
		txtSqlParser.setMaxSize(550, 150);
		Button btExecutar = new Button("Executar");
		btExecutar.setOnAction(e->executarQuery());
		btExecutar.setMinSize(70, 40);
		HBox hbQuery = new HBox(5);
		hbQuery.setAlignment(Pos.CENTER);
		hbQuery.getChildren().addAll(txtSqlParser, btExecutar);
		vboxConteudoParser.setPadding(new Insets(0, 0, 0, 5));
		
		vboxConteudoParser.getChildren().add(hbQuery);
		vboxConteudoParser.getChildren().add(criarTabelaTeste());
	}
	
	private void atualizarGrid(Node node)
	{
		if(grid.getChildren().contains(vboxConteudoParser))
			grid.getChildren().remove(vboxConteudoParser);
		if(grid.getChildren().contains(vboxNewTable))
			grid.getChildren().remove(vboxNewTable);
		if(grid.getChildren().contains(vboxGrid))
			grid.getChildren().removeAll(vboxGrid);
		
		grid.add(node, 1, 0);
	}
	
	private void mostrarJanelaNewTable()
	{
		atualizarGrid(vboxNewTable);
	}
	

	private void excluirTabela()
	{
		TreeItem<String> node = treeView.getSelectionModel().getSelectedItem();

	    if (node == null) {
	        return;
	    }

	    TreeItem<String> parent = node.getParent();
	    
	    if (parent != null) {
	        parent.getChildren().remove(node);
	        for (int i = 0; i < controlador.getBancos().get(0).getTabelas().size(); i++) 
	        {
	        	if(controlador.getBancos().get(0).getTabelas().get(i).getNome().equals(node.getValue()))
	        	{
	        		controlador.getBancos().get(0).getTabelas().get(i).deletarTabela();
	        		controlador.getBancos().get(0).getTabelas().remove(i);
	        		System.out.println("here");
	        	}
			}
	        atualizarGrid(new VBox());
	    } else {
	        //how to delete root item without parent?           
	    }
	}

	
	private void mostrarJanelaTable(String nomeBanco, String nomeTabela)
	{
		janelaTabela.atualizar(nomeBanco, nomeTabela);
		vboxGrid = new VBox(10);
		HBox hbox = new HBox(10);
		
		Label label = new Label("Nome tabela:");
		
		txtNomeTabela = new TextField(treeView.getSelectionModel().getSelectedItem().getValue());
		
		bExcluirTabela = new Button("Excluir Tabela");
		bExcluirTabela.setOnAction(e -> excluirTabela());
		bAtualizarNomeTabela = new Button("Atualizar");
		bAtualizarNomeTabela.setOnAction(e -> atualizarNomeTabela());
		
		hbox.getChildren().addAll(bExcluirTabela, label, txtNomeTabela, bAtualizarNomeTabela);
		
		vboxGrid.getChildren().addAll(hbox, painelJanelaTable);
		atualizarGrid(vboxGrid);
	}
	
	private void atualizarNomeTabela()
	{
		TreeItem<String> node = treeView.getSelectionModel().getSelectedItem();

	    if (node == null) {
	        return;
	    }

	    TreeItem<String> parent = node.getParent();
	    if (parent != null) {
	     
	        for (int i = 0; i < controlador.getBancos().get(0).getTabelas().size(); i++) 
	        {	
	        	if(controlador.getBancos().get(0).getTabelas().get(i).getNome().equals(node.getValue()))
	        	{
	        		controlador.getBancos().get(0).getTabelas().get(i).atualizarNomeTabela(txtNomeTabela.getText());
	        		controlador.getBancos().get(0).getTabelas().get(i).setNome(txtNomeTabela.getText());
	        	}
			}
	        treeView.getSelectionModel().getSelectedItem().setValue(txtNomeTabela.getText());
	        atualizarGrid(new VBox());
	    } else {
	        //how to delete root item without parent?           
	    }
	}
	
	private TableView criarTabelaTeste()
	{
		String[][] dadosMatriz = {{"Lucas", "21", "Masculino"}, {"Vanessa", "24", "Feminino"}, {"Joao", "40", "Masculino"}};
		String[] nomeColunas = {"Nome", "Idade", "Sexo"};
		
		return TabelaDinamica.gerarTabela(nomeColunas, dadosMatriz, 3);
	}
	
	public TreeView<String> carregarTreeView()
	{
		TreeItem<String> itemBds = controlador.getConteudoTreeView();
		TreeView<String> tree = new TreeView<String>(itemBds);
		
		tree.setMinSize(150, 600);
		tree.setMaxSize(150, 600);
		tree.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if(event.getClickCount() == 2)
				{
					TreeItem<String> item = tree.getSelectionModel().getSelectedItem();
					TreeItem<String> itemPai = item.getParent();
					if(itemPai != null)
					{
						if(!itemPai.getValue().equals("Banco de dados"))
						{
							String bancoDeDados = itemPai.getValue();
							String tabelaSelecioanda = item.getValue();
							if(tabelaSelecioanda.equals("New"))
							{
								bdSelecionado = bancoDeDados;
								// abre janela pra criar uma tabela
								mostrarJanelaNewTable();
							}
							else
							{
								// abre a estrutra da tabela
								mostrarJanelaTable(bancoDeDados, tabelaSelecioanda);
							}
							System.out.println("BD: " + bancoDeDados + "; Tabela: " + tabelaSelecioanda);
						}
						
					}	
				}
			}
		});
		return tree;
	}
	
	
	public void executarQuery()
	{
		//controlador.executarQuery(txtSqlParser.getText() );
	}

	public TreeView<String> getTreeView() {
		return treeView;
	}

	public void setTreeView(TreeView<String> treeView) {
		this.treeView = treeView;
	}
	
}
