package br.edu.iftm.models.pda;

import java.util.ArrayList;

public class AutomatoPilha {
	private PDAPilha pilha;
	private ArrayList<PDAEstado> estados;
	boolean mostrarLog;
	
	public AutomatoPilha(boolean mostrarLog)
	{
		pilha = new PDAPilha(100);
		estados = new ArrayList<PDAEstado>();
		this.mostrarLog = mostrarLog;
	}
	
	public void imprimir(String log)
	{
		if(mostrarLog)
			System.out.println(log);
	}
	
	public void addEstado(PDAEstado estado)
	{
		estados.add(estado);
	}
	
	private PDAEstado getInicial()
	{
		imprimir("getInicial()");
		for(PDAEstado estado : estados)
		{
			if(estado.isInicial())
				return estado;
		}
		
		return null;
	}
	
	private String adicionarFinalizadorNaFita(String fita)
	{
		return fita + PDAVertice.VALOR_NULO;
	}
	
	
	
	public boolean testar(String fita)
	{
		PDAEstado estadoInicial = getInicial();
		if(estadoInicial == null)
		{
			imprimir("Nao encontrou estado inicial!");
			return false;
		}
		
		
		fita = adicionarFinalizadorNaFita(fita);
		imprimir("Tamanho fita: "+fita.length());

		return testar(fita, estadoInicial);
	}
	
	private boolean gerarResultado(PDAEstado estado, PDAPilha pilha)
	{
		if(estado.isFinal())
		{
			imprimir("Chegou em um estado final");
			if(pilha.estaVazia())
			{
				imprimir("A pilha nao esta vazia!");
				return true;
			}
			else
				return false;
		}
		else
		{
			imprimir("Nao chegou em um estado final!");
			return false;
		}
		
	}
	
	private boolean testar(String fita, PDAEstado estado)
	{
		boolean sair = false, sucesso = false;
		PDAVertice vertice;
		int indexFita = 0;
		while(!sair)
		{
			char valorLidaFita = fita.charAt(indexFita++);
			imprimir("Valor atual na fita: '" + valorLidaFita + "'");
			vertice = buscarVertice(estado, valorLidaFita);
			imprimir("Estado: " +estado+" Vertice: ["+ vertice + "]");
			
			if(vertice == null)
			{
				sair = true;
				imprimir("Vertice == null");
			}
			else
			{
				if(pilha.push(vertice.getPush()) && pilha.pop(vertice.getPop()))
				{
					estado = vertice.getDestino();
					
					if(chegouFinalFita(estado, indexFita, fita))
					{	
						sucesso = gerarResultado(estado, pilha);
						sair = true;
					}
				}
				else
				{
					sair = true;
					imprimir("Nao foi possivel dar push/pop!");
				}
			}
		}
		return sucesso;
	}
	
	private boolean chegouFinalFita(PDAEstado estado, int index, String fita)
	{
		imprimir("Chegou no final da fita. Verificar se o estado "+estado+" e final");
		if(index == fita.length())
			return true;
		else
			return false;
	}
	
	private PDAVertice buscarVertice(PDAEstado estado, char valorLidoFita)
	{
		for(PDAVertice vertice : estado.getVertices())
		{
			if(vertice.getValorLidoFita() == valorLidoFita)
				return vertice;
		}
		
		imprimir("Nao encontrou um vertice com o valor " + valorLidoFita);
		return null;
	}

	public ArrayList<PDAEstado> getEstados() {
		return estados;
	}

	public void setEstados(ArrayList<PDAEstado> estados) {
		this.estados = estados;
	}
	
	
}
