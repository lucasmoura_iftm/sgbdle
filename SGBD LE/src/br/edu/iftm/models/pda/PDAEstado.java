package br.edu.iftm.models.pda;

import java.util.ArrayList;

public class PDAEstado {
	// Pushdown Automaton
	private String nome;
	private boolean tipoFinal, tipoInicial;
	private ArrayList<PDAVertice> vertices;
	
	public PDAEstado(String nome, boolean isInicial, boolean isFinal)
	{
		vertices = new ArrayList<PDAVertice>();
		this.nome = nome;
		this.tipoInicial = isInicial;
		this.tipoFinal = isFinal;
	}
	
	public void addVertice(PDAVertice vertice)
	{
		vertices.add(vertice);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isFinal() {
		return tipoFinal;
	}

	public void setTipoFinal(boolean tipoFinal) {
		this.tipoFinal = tipoFinal;
	}

	public boolean isInicial() {
		return tipoInicial;
	}

	public void setTipoInicial(boolean tipoInicial) {
		this.tipoInicial = tipoInicial;
	}

	public ArrayList<PDAVertice> getVertices() {
		return vertices;
	}

	public void setVertices(ArrayList<PDAVertice> vertices) {
		this.vertices = vertices;
	}
	
	public String toString()
	{
		return nome;
	}
}
