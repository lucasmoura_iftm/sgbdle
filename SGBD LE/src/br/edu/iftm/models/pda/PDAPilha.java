package br.edu.iftm.models.pda;

public class PDAPilha {
	private char[] pilha;
	private int tamanho, i;
	
	public PDAPilha(int tamanho)
	{
		this.tamanho = tamanho;
		i = 0;
		pilha = new char[tamanho];
	}
	
	public boolean estaVazia()
	{
		return i == 0;
	}
	
	public boolean push(char conteudo)
	{
		if(conteudo == PDAVertice.VALOR_NULO)
			return true;
		
		if(i < tamanho-1)
		{
			pilha[i++] = conteudo;
			return true;
		}
		else
			System.out.println("Voce ultrapassou o tamanho da pilha!");
		
		return false;
	}
	
	public boolean pop(char conteudo)
	{
		if(i == 0)
		{
			if(conteudo != PDAVertice.VALOR_NULO)
			{
				System.out.println("Nao ha nada na pilha.");
				return false;
			}
			else
			{
				return true;
			}
		}
		
		if(conteudo == PDAVertice.VALOR_NULO)
		{
			return true;
		}
		else if(pilha[i-1] == conteudo)
		{
			pilha[i--] = ' ';
			return true;
		}
		else
		{
			System.out.println("No topo da pilha n�o tem " + conteudo);
			return false;
		}
	}
	
	public void imprimir()
	{
		for(int j=0; j < i; j++)
		{
			System.out.println("["+j+"] = '"+pilha[j]+"'");
		}
	}
	
	
}
