package br.edu.iftm.models.pda;

public class AutomatoParenteses {
	public static boolean testar(String conteudoFita)
	{
		AutomatoPilha automato = new AutomatoPilha(false);
		PDAEstado estadoA = new PDAEstado("A", true, true);
		PDAEstado estadoB = new PDAEstado("B", false, false);
		PDAEstado estadoC = new PDAEstado("C", false, false);
		PDAEstado estadoD = new PDAEstado("D", false, true);
		estadoA.addVertice(new PDAVertice(estadoB, '(', PDAVertice.VALOR_NULO, '('));
		estadoB.addVertice(new PDAVertice(estadoB, '(', PDAVertice.VALOR_NULO, '('));
		estadoB.addVertice(new PDAVertice(estadoC, ')', '(', PDAVertice.VALOR_NULO));
		estadoC.addVertice(new PDAVertice(estadoC, ')', '(', PDAVertice.VALOR_NULO));
		estadoC.addVertice(new PDAVertice(estadoB, '(', PDAVertice.VALOR_NULO, '('));
		estadoC.addVertice(new PDAVertice(estadoD, PDAVertice.VALOR_NULO, PDAVertice.VALOR_NULO, PDAVertice.VALOR_NULO));
		automato.addEstado(estadoA);
		automato.addEstado(estadoB);
		automato.addEstado(estadoC);
		automato.addEstado(estadoD);
		return automato.testar(conteudoFita);
	}
}
