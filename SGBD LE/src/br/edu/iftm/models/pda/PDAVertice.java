package br.edu.iftm.models.pda;

public class PDAVertice {
	private char valorLidoFita, pop, push;
	private PDAEstado destino;
	public static final char VALOR_NULO = (char)1;
	
	/**
	 * @param destino Destino
	 * @param fita Sub-string que ser� lida na fita
	 * @param pop Sub-string que ser� retirada da pilha
	 * @param push Sub-string que ser� adicionada na pilha
	 */
	public PDAVertice(PDAEstado destino, char valorLidoFita, char pop, char push) {
		this.valorLidoFita = valorLidoFita;
		this.pop = pop;
		this.push = push;
		this.destino = destino;
	}


	public char getValorLidoFita() {
		return valorLidoFita;
	}


	public void setValorLidoFita(char valorLidoFita) {
		this.valorLidoFita = valorLidoFita;
	}


	public char getPop() {
		return pop;
	}

	public void setPop(char pop) {
		this.pop = pop;
	}

	public char getPush() {
		return push;
	}

	public void setPush(char push) {
		this.push = push;
	}

	public PDAEstado getDestino() {
		return destino;
	}

	public void setDestino(PDAEstado destino) {
		this.destino = destino;
	}
	
	public String toString()
	{
		return valorLidoFita + "," + pop + "," + push;
	}
	
}
