package br.edu.iftm.models;

import java.io.File;import java.io.LineNumberInputStream;
import java.nio.file.Files;
import java.util.ArrayList;

public class Banco {
	private String nome;
	public static final String BD_FOLDER = "BDs";
	private ArrayList<Tabela> tabelas;
	
	public Banco(String nome)
	{
		this.nome = nome;
		tabelas = new ArrayList<Tabela>();
		criarBanco();
	}
	
	private void criarBanco()
	{
		new File(BD_FOLDER+"/"+nome).mkdirs();
	}
	
	public void addTabela(Tabela tabela)
	{
		tabelas.add(tabela);
	}
	
	public static ArrayList<Banco> carregarBancos()
	{
		ArrayList<Banco> bancos = new ArrayList<Banco>();
		
		File folder = new File(BD_FOLDER);
		File[] listaPastasBd = folder.listFiles();
		
		for(File pastaBd : listaPastasBd)
		{
			Banco novoBanco = new Banco(pastaBd.getName());
			File[] listaArquivosTabelas = pastaBd.listFiles();
			novoBanco.setTabelas(Tabela.carregarTabelas(pastaBd));
			bancos.add(novoBanco);
		}
		
		return bancos;
	}
	
	public static int getIdBanco(ArrayList<Banco> bancos, String nomeBanco)
	{
		for(int i=0; i < bancos.size(); i++)
		{
			if(bancos.get(i).getNome().equalsIgnoreCase(nomeBanco))
				return i;
		}
		
		Erro.gerarErro("Nao encontrou o banco " + nomeBanco +".");
		return -1;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public ArrayList<Tabela> getTabelas() {
		return tabelas;
	}

	public void setTabelas(ArrayList<Tabela> tabelas) {
		this.tabelas = tabelas;
	}
	
	
	
}
