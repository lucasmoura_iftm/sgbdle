package br.edu.iftm.models;

public class ConteudoConsulta {
	private String[] nomeColunas;
	private String[][] valores;
	private int indiceValor = 0, indiceColuna = 0, qtdColunas, qtdValores;
	
	public ConteudoConsulta(int qtdColunas, int qtdValores)
	{
		nomeColunas = new String[qtdColunas];
		valores = new String[qtdValores][qtdColunas];
		this.qtdColunas = qtdColunas;
		this.qtdValores = qtdValores;
	}
	
	public void addValor(String[] registroValores)
	{
		if(indiceValor < qtdValores)
			valores[indiceValor++] = registroValores;
	}
	
	public void addColuna(String nomeColuna)
	{
		if(indiceColuna < qtdColunas)
			nomeColunas[indiceColuna++] = nomeColuna;
	}
	

	public String[] getNomeColunas() {
		return nomeColunas;
	}

	public void setNomeColunas(String[] nomeColunas) {
		this.nomeColunas = nomeColunas;
	}

	public String[][] getValores() {
		return valores;
	}

	public void setValores(String[][] valores) {
		this.valores = valores;
	}

	public int getQtdColunas() {
		return qtdColunas;
	}

	public void setQtdColunas(int qtdColunas) {
		this.qtdColunas = qtdColunas;
	}

	public int getQtdValores() {
		return qtdValores;
	}

	public void setQtdValores(int qtdValores) {
		this.qtdValores = qtdValores;
	}
	
	public int getIndiceValor()
	{
		return indiceValor;
	}
}
