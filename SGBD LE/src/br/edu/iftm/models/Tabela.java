package br.edu.iftm.models;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Tabela {
	private String nome, banco;
	private ArrayList<Campo> campos;
	private File file;
	private String diretorio;
	private ArrayList<Registro> registros;
	public static final String VALOR_NULL = "null";
	public static final int INDICE_COLUNA_NAO_ENCONTRADA = -1;
	
	
	public Tabela(String nome, String banco) 
	{
		this.nome = nome;
		this.campos = new ArrayList<Campo>();
		this.banco = banco;
		this.registros = new ArrayList<Registro>();
		diretorio = Banco.BD_FOLDER+File.separator+banco+File.separator;
	}
	

	public Tabela(String nome, String banco, ArrayList<Campo> campos) 
	{
		this.nome = nome;
		this.campos = campos;
		this.banco = banco;
		this.registros = new ArrayList<Registro>();
		diretorio = Banco.BD_FOLDER+File.separator+banco+File.separator;
	}
	
	public static ArrayList<Tabela> carregarTabelas(File pastaBd)
	{
		ArrayList<Tabela> tabelas = new ArrayList<Tabela>();
		File[] listaArquivosTabelas = pastaBd.listFiles();
		for(File arquivoTabela : listaArquivosTabelas)
		{
			int i = arquivoTabela.getName().lastIndexOf('.');
			String nomeArquivo = arquivoTabela.getName().substring(0, i);
			String extensao = arquivoTabela.getName().substring(i+1);
			if(extensao.equals("h"))
			{
				System.out.println("Tabela: " + arquivoTabela.getName().substring(0, i));
				Tabela novaTabela = new Tabela(nomeArquivo, pastaBd.getName());
				novaTabela.setCampos(Campo.carregarCampos(arquivoTabela, novaTabela.getBanco(), novaTabela.getNome()));
				System.out.println("Qnt de campos carregados: " + novaTabela.getCampos().size());
				System.out.println("novaTabela sizeCampos: " + novaTabela.getCampos().size());
				File arquivoRegistros = Arquivo.procurarArquivoNoVetor(listaArquivosTabelas, nomeArquivo+".le");
				novaTabela.setRegistros(Registro.carregarRegistros(arquivoRegistros, novaTabela.getCampos()));
				tabelas.add(novaTabela);
			}
		}
		
		return tabelas;
	}
	

	public void addRegistro(ArrayList<String> colunas, ArrayList<String> conteudoRegistro)
	{
		Registro novoRegistro = new Registro();
		boolean gerouErro = false;
		String novoValorCampo;
		int indiceCampo = 0;

		for(Campo campo : campos)
		{
			novoValorCampo = verificarEGerarValorCampo(campo, indiceCampo, colunas, conteudoRegistro);
			if(novoValorCampo == null)
				gerouErro = true;
			else
				novoRegistro.addColuna(novoValorCampo);

			indiceCampo++;
		}
		
		if(gerouErro)
			return;
		
		registros.add(novoRegistro);
		String stringRegistro = "";
		
		for(String coluna : novoRegistro.getConteudos())
			stringRegistro += coluna;
		
		escreverNaTabela(stringRegistro);
	}
	
	private int buscarIndiceColuna(ArrayList<String> colunas, Campo campo)
	{
		int indiceColuna = INDICE_COLUNA_NAO_ENCONTRADA;
		
		for(int j=0; j < colunas.size(); j++)
		{
			if(colunas.get(j).equalsIgnoreCase(campo.getNome()))
				indiceColuna = j;
		}
		
		return indiceColuna;
	}
	
	private String verificarEGerarValorCampo(Campo campo, int indiceCampo, ArrayList<String> colunas, ArrayList<String> conteudoRegistro)
	{
		String valorCampo = null;
		int indiceColuna = buscarIndiceColuna(colunas, campo);
		
		if(indiceColuna == INDICE_COLUNA_NAO_ENCONTRADA)
			valorCampo = getValorNulo(campo);
		else
			valorCampo = getValorNaoNulo(campo, conteudoRegistro.get(indiceColuna), indiceCampo);
		
		if(valorCampo == null)
			return null;
		else
			return valorCampo;

	}
	
	private String getValorNulo(Campo campo)
	{
		String valor = Restricoes.getValorSeValorEntradaNulo(campo);
		if(!valor.equals(Restricoes.VALOR_ERRO))
			return valor;
		else
			return null;
	}
	
	private String getValorNaoNulo(Campo campo, String valorNaoConvertido, int indiceCampo)
	{
		if(Restricoes.verificarSeValorSegueRegrasSigned(valorNaoConvertido, campo))
		{
			if(campo.getRestricoes().isPrimary() || campo.getRestricoes().isUnique())
			{
				if(Restricoes.verificarSeValorIsUnico(converterPraColuna(valorNaoConvertido, campo), registros, indiceCampo))
				{
					return converterPraColuna(valorNaoConvertido, campo);
				}
				else
				{
					Erro.gerarErro("O valor("+valorNaoConvertido+") do campo "+campo.getNome()+" deve ser unico!");
					return null;
				}
			}
			else
			{
				return converterPraColuna(valorNaoConvertido, campo);
			}
				
		}
		return null;
	}
	
	public void atualizarNomeTabela(String novoNome)
	{
		Arquivo.mudarNomeTabela(diretorio + nome + ".le",diretorio + novoNome + ".le");
		Arquivo.mudarNomeTabela(diretorio + nome + ".h",diretorio + novoNome + ".h");
	}
	
	private String converterPraColuna(String valorCampo, Campo campo)
	{
		if(valorCampo.equals(VALOR_NULL))
		{
			return campo.getTipo().converter();
		}
		else
		{	
			return campo.getTipo().converter(valorCampo);
		}	
	}
	
	private boolean jaExisteTabela()
	{
		file = new File(diretorio+nome+".le");
		return file.exists();
	}
	
	public boolean criarTabela()
	{
		if(!jaExisteTabela())
		{
			criarArquivosTabela();
			criarCampos();
			return true;
		}
		else
		{
			System.out.println("Ja existe a tabela!");
			return false;
		}
	}
	
	private void criarCampos()
	{
		for (Campo campo : campos) {
			// adicionada as informações no arquivo cabeçalho
			// e no conteudo
			//System.out.println("Conteudo: " + campo.gerarCampo());
			//escreverNaTabela(campo.gerarCampo());
			System.out.println("Cabecalho: " + campo.gerarCabecalho());
			escreverNoCabecalho(campo.gerarCabecalho());
		}
	}
	
	private void escreverNaTabela(String conteudo)
	{
		try {
			FileWriter fileWriter = new FileWriter(diretorio+nome+".le", true);
			BufferedWriter bufferWritter = new BufferedWriter(fileWriter);
			bufferWritter.write(conteudo);
			bufferWritter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private void escreverNoCabecalho(String conteudo)
	{
		try {
			FileWriter fileWriter = new FileWriter(diretorio+nome+".h", true);
			BufferedWriter bufferWritter = new BufferedWriter(fileWriter);
			bufferWritter.write(conteudo+"\n");
			bufferWritter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deletarTabela()
	{
		Arquivo.apagarArquivo(diretorio + nome + ".le");
		Arquivo.apagarArquivo(diretorio + nome + ".h");
	}
	
	private void criarArquivosTabela()
	{
		if(Arquivo.criarArquivo(diretorio + nome + ".le"))
			Arquivo.criarArquivo(diretorio + nome + ".h");
		else
			System.out.println("O arquivo "+nome+".le ja existe!");
	}
	
	public static int getIdTabela(ArrayList<Tabela> tabelas, String nomeTabela)
	{
		for(int i=0; i < tabelas.size(); i++)
		{
			if(tabelas.get(i).getNome().equalsIgnoreCase(nomeTabela))
				return i;
		}
		
		Erro.gerarErro("Nao encontrou a tabela " + nomeTabela +".");
		return -1;
	}
	
	
	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public ArrayList<Campo> getCampos() {
		return campos;
	}

	public void setCampos(ArrayList<Campo> campos) {
		this.campos = campos;
	}

	public ArrayList<Registro> getRegistros() {
		return registros;
	}

	public void setRegistros(ArrayList<Registro> registros) {
		this.registros = registros;
	}
	
	
}
