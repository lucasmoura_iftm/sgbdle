package br.edu.iftm.models;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.edu.iftm.models.tipos.Tipo;
import br.edu.iftm.models.tipos.TipoChar;
import br.edu.iftm.models.tipos.TipoDate;
import br.edu.iftm.models.tipos.TipoDateTime;
import br.edu.iftm.models.tipos.TipoFloat;
import br.edu.iftm.models.tipos.TipoInt;
import br.edu.iftm.models.tipos.TipoText;
import br.edu.iftm.models.tipos.TipoVarChar;

public class Campo {
	private String nome, conteudo, nomeBanco, nomeTabela;
	private int indiceCampo;
	private Tipo tipo;
	private Restricoes restricoes;
	
	
	public Campo(String nomeBanco, String nomeTabela, int indiceCampo)
	{
		this.nomeBanco = nomeBanco;
		this.nomeTabela = nomeTabela;
		this.indiceCampo = indiceCampo;
		restricoes = new Restricoes(nomeBanco, nomeTabela, indiceCampo);
	}
	
	public Campo(String nome, Tipo tipo, String nomeBanco, String nomeTabela, int indiceCampo) {
		this.nome = nome;
		this.tipo = tipo;
		this.nomeBanco = nomeBanco;
		this.nomeTabela = nomeTabela;
		this.indiceCampo = indiceCampo;
		restricoes = new Restricoes(nomeBanco, nomeTabela, indiceCampo);
	}
	
	public Campo(String nome, Tipo tipo, Restricoes restricoes) {
		this.nome = nome;
		this.tipo = tipo;
		this.restricoes = restricoes;
	}
	
	public String gerarCabecalho()
	{
		String cabecalho = "'"+nome+"' ";
		cabecalho += tipo + "("+tipo.getTamanho()+") = {" + restricoes + "}";
		
		return cabecalho;
	}
	
	public String gerarCampo()
	{
		return tipo.converter(conteudo);

	}
	
	public static ArrayList<Campo> carregarCampos(File cabecalhoTabela, String nomeBanco, String nomeTabela)
	{
		ArrayList<Campo> campos;
		String conteudoCabecalho = Arquivo.lerArquivo(cabecalhoTabela);
		System.out.println(conteudoCabecalho);
		campos = gerarCamposPelaString(conteudoCabecalho, nomeBanco, nomeTabela);
		System.out.println("Retonando campos");
		System.out.println("Tipo primeiro campo: " + campos.get(0).getNome());
		return campos;
	}
	
	private static ArrayList<Campo> gerarCamposPelaString(String conteudoArquivo, String nomeBanco, String nomeTabela)
	{
		ArrayList<Campo> campos = new ArrayList<Campo>();
		String[] stringCampos = conteudoArquivo.split(System.lineSeparator());
		for(String linhaCampo : stringCampos)
		{
			campos.add(gerarCampo(linhaCampo, nomeBanco, nomeTabela, campos.size()));
		}
	
		return campos;
	}
	
	private static Campo gerarCampo(String linhaCampo, String nomeBanco, String nomeTabela, int indiceCampo)
	{
		String nome;
		Tipo tipo;
		int tamanho;
		Restricoes restricoes;
		
		Campo novoCampo = new Campo(nomeBanco, nomeTabela, indiceCampo);
		Pattern padrao = Pattern.compile("(')([\\w\\W]+)(') (\\w+)\\((\\d+)\\) = \\{([\\w=;.-]+)\\}");
		Matcher matcher = padrao.matcher(linhaCampo);
		if(matcher.find())
		{
			//System.out.println("Qtd de grupos: " + matcher.groupCount());
			nome = matcher.group(2);
			novoCampo.setNome(nome);
			//System.out.println("Nome: " + nome);
			//System.out.println("Tipo: " + matcher.group(4));
			tamanho = Integer.parseInt(matcher.group(5));
			//System.out.println("Tamanho: " + tamanho);
			tipo = getTipo(matcher.group(4), tamanho);
			novoCampo.setTipo(tipo);
			//System.out.println("Restricoes: " + matcher.group(6));
			System.out.println("Gerando restricoes!");
			restricoes = Restricoes.gerarRestricoes(matcher.group(6), nomeBanco, nomeTabela, indiceCampo);
			novoCampo.setRestricoes(restricoes);
		}

		return novoCampo;
	}
	
	private static Tipo getTipo(String nomeTipo, int tamanho)
	{
		Tipo tipoGerado = null;

		switch (nomeTipo) 
		{
			case TipoChar.CABECALHO:
				tipoGerado = new TipoChar(tamanho);
				break;
			case TipoDate.CABECALHO:
				tipoGerado = new TipoDate();
				break;
			case TipoDateTime.CABECALHO:
				tipoGerado = new TipoDateTime();
				break;
			case TipoFloat.CABECALHO:
				tipoGerado = new TipoFloat();
				break;
			case TipoInt.CABECALHO:
				tipoGerado = new TipoInt();
				break;
			case TipoText.CABECALHO:
				tipoGerado = new TipoText();
				break;
			case TipoVarChar.CABECALHO:
				tipoGerado = new TipoVarChar(tamanho);
				break;
		}
		
		return tipoGerado;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public Restricoes getRestricoes() {
		return restricoes;
	}

	public void setRestricoes(Restricoes restricoes) {
		this.restricoes = restricoes;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	
	
	
}
