package br.edu.iftm.models.tipos;

public class TipoDate extends Tipo{
	public static final String CABECALHO = "date";
	public static final String VALOR_NULL = "0000-00-00";
	
	public TipoDate()
	{
		super(10);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return CABECALHO;
	}

	@Override
	public String converter(String conteudo) {
		return conteudo;
	}

	@Override
	public String converter() {
		// TODO Auto-generated method stub
		return converter(VALOR_NULL);
	}

	@Override
	public String[] obterValor(String linha) {
		// TODO Auto-generated method stub
		String[] resultado = new String[2];
		resultado[INDICE_VALOR] = linha.substring(INICIO, getTamanho());
		resultado[INDICE_LINHA] = linha.substring(getTamanho());
		return resultado;
	}

	@Override
	protected String desconverter(String valorFormatoArquivo) {
		// TODO Auto-generated method stub
		return valorFormatoArquivo;
	}
	
}
