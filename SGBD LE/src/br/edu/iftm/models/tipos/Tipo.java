package br.edu.iftm.models.tipos;

public abstract class Tipo {
	private int tamanho;
	public static final int INICIO = 0;
	public static final int INDICE_VALOR = 0;
	public static final int INDICE_LINHA = 1;
	
	public Tipo(int tamanho) {
		this.tamanho = tamanho;
	}
	
	public abstract String toString();
	public abstract String converter();
	public abstract String converter(String conteudo);
	protected abstract String desconverter(String valorFormatoArquivo);
	
	/**
	 * @param linha Texto contendo todos os registros
	 * @param tamanhoNoCabecalho Tamanho do valor informado no cabecalho da tabela
	 * @return Retorna um vetor de String contendo a valor obtido e no outro, a nova linha de registros sem o valor obtido
	 */
	public abstract String[] obterValor(String linha); 
	
	public static Tipo retornaTipo(String tipo, int tamanho)
	{
		switch (tipo) 
		{
			case "INT":
				return new TipoInt();
			case "CHAR":
				return new TipoChar(tamanho);
			case "VARCHAR":
				return new TipoVarChar(tamanho);
			case "DATE":
				return new TipoDate();
			case "DATETIME":
				return new TipoDateTime();
			case "FLOAT":
				return new TipoFloat();
			case "TEXT":
				return new TipoText();
			default:
				return null;
		}
	}
	
	public int getTamanho() {
		return tamanho;
		
	}

	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}
	
	
}
