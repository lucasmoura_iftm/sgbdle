package br.edu.iftm.models.tipos;

public class TipoVarChar extends Tipo{
	public static final String CABECALHO = "varchar";
	private final char CHAR_TAMANHO = 1;
	public static final String VALOR_NULL = "n";
	
	public TipoVarChar(int tamanho)
	{
		super(tamanho);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return CABECALHO;
	}

	@Override
	public String converter(String conteudo) {
		if(conteudo.length() > getTamanho() && conteudo.length() <= 255)
		{
			System.out.println("O conteudo ultrapassou o tamanho maximo.");
			return null;
		}
		
		
		
		String s = ""+ ((char) CHAR_TAMANHO) + conteudo.length() + ((char) CHAR_TAMANHO);
		System.out.println("Converter para varchar, resultado de '"+conteudo+"' para '" + s +"'");
		
		s += conteudo;
		
		return s;
	}

	@Override
	public String converter() {
		// TODO Auto-generated method stub
		return converter(VALOR_NULL);
	}

	@Override
	public String[] obterValor(String linha) {
		// TODO Auto-generated method stub
		String[] resultado = new String[2];
		int indexSegundoCharTamanho = -1;
		for(int i=1; i < linha.length(); i++)
		{
			if(linha.charAt(i) == ((char)CHAR_TAMANHO) && indexSegundoCharTamanho == -1)
				indexSegundoCharTamanho = i;
		}
		
		System.out.println("Indice segundo char: " + indexSegundoCharTamanho);
		int tamanho = Integer.parseInt(linha.substring(INICIO+1, indexSegundoCharTamanho));
		int indexInicioValor = indexSegundoCharTamanho+1;
		resultado[INDICE_VALOR] = linha.substring(indexInicioValor, indexInicioValor+tamanho);
		resultado[INDICE_LINHA] = linha.substring(indexInicioValor+tamanho);
		return resultado;
	}

	@Override
	protected String desconverter(String valorFormatoArquivo) {
		return valorFormatoArquivo;
	}
}
