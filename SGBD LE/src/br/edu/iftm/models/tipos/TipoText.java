package br.edu.iftm.models.tipos;

public class TipoText extends Tipo{
	public static final String CABECALHO = "text";
	private final char CHAR_TAMANHO = 2;
	public static final String VALOR_NULL = "null";
	
	public TipoText()
	{
		super(0);
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return CABECALHO;
	}

	@Override
	public String converter(String conteudo) {
		String s = ""+ ((char) CHAR_TAMANHO) + conteudo.length() + ((char) CHAR_TAMANHO);
		
		s += conteudo;
		
		return s;
	}

	@Override
	public String converter() {
		// TODO Auto-generated method stub
		return converter(VALOR_NULL);
	}

	@Override
	public String[] obterValor(String linha) {
		// TODO Auto-generated method stub
		String[] resultado = new String[2];
		int indexSegundoCharTamanho = -1;
		for(int i=1; i < linha.length(); i++)
		{
			if(linha.charAt(i) == ((char)CHAR_TAMANHO) && indexSegundoCharTamanho == -1)
				indexSegundoCharTamanho = i;
		}
		
		System.out.println("Indice segundo char: " + indexSegundoCharTamanho);
		int tamanho = Integer.parseInt(linha.substring(INICIO+1, indexSegundoCharTamanho));
		int indexInicioValor = indexSegundoCharTamanho+1;
		resultado[INDICE_VALOR] = linha.substring(indexInicioValor, indexInicioValor+tamanho);
		resultado[INDICE_LINHA] = linha.substring(indexInicioValor+tamanho);
		return resultado;
	}

	@Override
	protected String desconverter(String valorFormatoArquivo) {
		return valorFormatoArquivo;
	}

}
