package br.edu.iftm.models.tipos;

public class TipoChar extends Tipo{
	public static final String CABECALHO = "char";
	private final char CHAR_PREENCHER = 5;
	public static final String VALOR_NULL = "n";
	
	public TipoChar(int tamanho)
	{
		super(tamanho);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return CABECALHO;
	}

	@Override
	public String converter(String conteudo) {
		if(conteudo.length() > getTamanho() && conteudo.length() <= 255)
		{
			System.out.println("O conteudo ultrapassou o tamanho maximo.");
			return null;
		}
		
		// 10
		// a0000000
		
		String s = conteudo;
		for(int i=0; i < (getTamanho()-conteudo.length()); i++)
		{
			s += (char)CHAR_PREENCHER;
		}
		
		return s;
	}

	@Override
	public String converter() {
		// TODO Auto-generated method stub
		return converter(VALOR_NULL);
	}
	

	@Override
	public String[] obterValor(String linha) {
		// TODO Auto-generated method stub
		String[] resultado = new String[2];
		
		resultado[INDICE_VALOR] = desconverter(linha.substring(INICIO, getTamanho()));
		resultado[INDICE_LINHA] = linha.substring(getTamanho());
		return resultado;
	}

	@Override
	protected String desconverter(String valorFormatoArquivo) {
		String valor = "";
		valor = valorFormatoArquivo.replace(""+(char)CHAR_PREENCHER, "");
		return valor;
	}

}
