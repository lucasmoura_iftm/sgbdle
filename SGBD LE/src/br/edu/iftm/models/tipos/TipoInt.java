package br.edu.iftm.models.tipos;

public class TipoInt extends Tipo{
	public static final String CABECALHO = "int";
	public static final String VALOR_NULL = "0";
	
	public TipoInt()
	{
		super(6);
	}
	
	@Override
	public String toString() {
		return CABECALHO;
	}
	
	@Override
	public String converter(String conteudo)
	{
		if(conteudo.length() > getTamanho())
		{
			System.out.println("O conteudo ultrapassou o tamanho maximo.");
			return null;
		}
		
		String s = "";
		for(int i=0; i < (getTamanho()-conteudo.length()); i++)
		{
			s += "0";
		}
		s += conteudo;
		return s;
	}

	@Override
	public String converter() {
		// TODO Auto-generated method stub
		return converter(VALOR_NULL);
	}

	@Override
	public String[] obterValor(String linha) {
		// TODO Auto-generated method stub
		
		String[] resultado = new String[2];
		if(linha.charAt(0) == '-')
		{
			resultado[INDICE_VALOR] = ""+desconverter(linha.substring(INICIO, getTamanho()+1));
			resultado[INDICE_LINHA] = linha.substring(getTamanho()+1);
		}
		else
		{
			resultado[INDICE_VALOR] = ""+desconverter(linha.substring(INICIO, getTamanho()));
			resultado[INDICE_LINHA] = linha.substring(getTamanho());
		}
		
		return resultado;
	}

	@Override
	protected String desconverter(String valorFormatoArquivo) {
		String valor = "";
		Integer valorInt = Integer.parseInt(valorFormatoArquivo);
		return ""+valorInt;
	}



	

}
