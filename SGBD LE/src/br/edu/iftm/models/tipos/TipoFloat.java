package br.edu.iftm.models.tipos;

public class TipoFloat extends Tipo{
	public static final String CABECALHO = "float";
	public static final String VALOR_NULL = "0.0";
	
	public TipoFloat()
	{
		super(6);
	}
	
	@Override
	public String toString() {
		return CABECALHO;
	}

	@Override
	public String converter(String conteudo) {
		String s = "";
		
		String[] conteudos = conteudo.split("\\.");
		System.out.println("Tamanho " + conteudos.length);
		if(conteudos[0].length() > getTamanho() || conteudos[1].length() > getTamanho())
		{	System.out.println("O conteudo ultrapassou o tamanho maximo.");
			return null;
		}
		
		for(int i=0; i < (getTamanho()-conteudos[0].length()); i++)
		{
			s += "0";
		}
		s += conteudos[0] + "." + conteudos[1];
		
		for(int i=0; i < (getTamanho()-conteudos[1].length()); i++)
		{
			s += "0";
		}
		
		
		return s;
	}

	@Override
	public String converter() {
		// TODO Auto-generated method stub
		return converter(VALOR_NULL);
	}

	@Override
	public String[] obterValor(String linha) {
		// TODO Auto-generated method stub
		String[] resultado = new String[2];
		
		if(linha.charAt(0) == '-')
		{
			resultado[INDICE_VALOR] = ""+desconverter(linha.substring(INICIO, getTamanho()+1+getTamanho()+1));
			resultado[INDICE_LINHA] = linha.substring(getTamanho()+1+getTamanho()+1);
		}
		else
		{
			resultado[INDICE_VALOR] = ""+desconverter(linha.substring(INICIO, getTamanho()+1+getTamanho()));
			resultado[INDICE_LINHA] = linha.substring(getTamanho()+1+getTamanho());
		}
		return resultado;
	}

	@Override
	protected String desconverter(String valorFormatoArquivo) {
		String valor = "";
		Float valorFloat = Float.parseFloat(valorFormatoArquivo);
		return ""+valorFloat;
	}

}
