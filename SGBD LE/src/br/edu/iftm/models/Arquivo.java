package br.edu.iftm.models;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Arquivo {
	
	public static boolean criarArquivo(String nomeArquivo)
	{
		try {
			File file = new File(nomeArquivo);
			if(!file.exists())
			{
				file.createNewFile();
				return true;
			}
			else
			{
				return false;
			}
		} catch (IOException e) {
			System.out.println("Nao foi possivel criar o arquivo " + nomeArquivo);
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static boolean criarArquivo(File file)
	{
		try {
			if(!file.exists())
			{
				file.createNewFile();
				return true;
			}
			else
			{
				return false;
			}
		} catch (IOException e) {
			System.out.println("Nao foi possivel criar o arquivo " + file.getName());
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static String lerArquivo(File arquivo){
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(arquivo));
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			
			br.close();
			return sb.toString();
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return null;
	}
	
	public static void escreverNoArquivo(String conteudo, File arquivo, boolean append)
	{
		try {
			FileWriter fileWriter = new FileWriter(arquivo, append);
			BufferedWriter bufferWritter = new BufferedWriter(fileWriter);
			bufferWritter.write(conteudo);
			bufferWritter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Nao foi possivel escrever no arquivo " + arquivo);
			e.printStackTrace();
		}
	}
	
	public static void apagarArquivo(String caminho)
	{
		File file = new File(caminho);
		if(!file.delete())System.out.println("Arquivo n�o existe");;
	}
	
	public static void mudarNomeTabela(String caminho, String novoNome)
	{
		File file = new File(caminho);
		file.renameTo(new File(novoNome));
	}
	
	public static File procurarArquivoNoVetor(File[] listaArquivos, String nomeArquivo)
	{
		for(int i=0; i < listaArquivos.length; i++)
		{
			if(listaArquivos[i].getName().equalsIgnoreCase(nomeArquivo))
				return listaArquivos[i];
		}
		return null;
	}
}
