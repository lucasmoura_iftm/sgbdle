package br.edu.iftm.models;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.edu.iftm.models.tipos.TipoFloat;
import br.edu.iftm.models.tipos.TipoInt;

/**
 * @author Lucas
 *
 */
/**
 * @author Lucas
 *
 */
public class Restricoes {
	private boolean primary = false, autoIncrement = false, signed = false, nulo = false, unique = false;
	private String defaultValue, conteudo, nomeBanco, nomeTabela;
	private int contador, indiceCampo;
	public static final String VALOR_ERRO = "-1";
	public static final String VALOR_NULL = "null";

	public Restricoes(String nomeBanco, String nomeTabela, int indiceCampo) {
		this.nomeBanco = nomeBanco;
		this.nomeTabela = nomeTabela;
		this.indiceCampo = indiceCampo;
		contador = 1;
	}

	public Restricoes(boolean primary, boolean autoIncrement, boolean unique, String nomeBanco, String nomeTabela, int indiceCampo) {
		this.primary = primary;
		this.autoIncrement = autoIncrement;
		this.unique = unique;
		this.nomeBanco = nomeBanco;
		this.nomeTabela = nomeTabela;
		this.indiceCampo = indiceCampo;
		contador = 1;
	}

	public Restricoes(boolean primary, boolean autoIncrement, boolean signed, boolean nulo, boolean unique,
			String defaultValue) {
		this.primary = primary;
		this.autoIncrement = autoIncrement;
		this.signed = signed;
		this.nulo = nulo;
		this.unique = unique;
		this.defaultValue = defaultValue;
		contador = 1;
	}
	
	public void aumentarContador()
	{
		contador++;
		System.out.println("Atualizar contador. Nome bd: "+nomeBanco+"; Nome tabela: "+nomeTabela+";IndiceCampo:"+indiceCampo);
		String diretorio = Banco.BD_FOLDER+File.separator+nomeBanco+File.separator;
		File arquivoCabecalho = new File(diretorio+nomeTabela+".h");
		String conteudoCabecalho = Arquivo.lerArquivo(arquivoCabecalho);
		String conteudoCampoDesejadoAntigo = conteudoCabecalho.split(System.lineSeparator())[indiceCampo];
		System.out.println("Conteudo desejado: " + conteudoCampoDesejadoAntigo);
		String conteudoCampoDesejadoNovo = conteudoCampoDesejadoAntigo.replaceFirst("(ai-value=)(\\d+)", "$1"+contador);
		conteudoCabecalho = conteudoCabecalho.replace(conteudoCampoDesejadoAntigo, conteudoCampoDesejadoNovo);
		Arquivo.escreverNoArquivo(conteudoCabecalho, arquivoCabecalho, false);
	}


	public static Restricoes gerarRestricoes(String stringRestricao, String nomeBanco, String nomeTabela, int indiceCampo)
	{
		Restricoes restricoes = new Restricoes(nomeBanco, nomeTabela, indiceCampo);
		Pattern padrao = Pattern.compile("isPrimary=(\\w+);isAutoincrement=(\\w+);(ai-value=(\\d+);)?isSigned=(\\w+);isNulo=(\\w+);isUnique=(\\w+);defaultValue=([\\w\\W]+)");
		Matcher matcher = padrao.matcher(stringRestricao);
		if(matcher.find())
		{
			System.out.println("isPrimary = " + matcher.group(1));
			restricoes.setPrimary(Boolean.parseBoolean(matcher.group(1)));
			System.out.println("isAutoincrement= " + matcher.group(2));
			restricoes.setAutoIncrement(Boolean.parseBoolean(matcher.group(2)));
			if(restricoes.isAutoIncrement())
			{
				System.out.println("ai-value= " + matcher.group(4));
				restricoes.setContador(Integer.parseInt(matcher.group(4)));
			}
			System.out.println("isSigned= " + matcher.group(5));
			restricoes.setSigned(Boolean.parseBoolean(matcher.group(5)));
			System.out.println("isNulo= " + matcher.group(6));
			restricoes.setNulo(Boolean.parseBoolean(matcher.group(6)));
			System.out.println("isUnique = " + matcher.group(7));
			restricoes.setUnique(Boolean.parseBoolean(matcher.group(7)));
			System.out.println("defaultValue= " + matcher.group(8));
			restricoes.setDefaultValue(matcher.group(8));
		}
		else
		{
			Erro.gerarErro("Nao conseguiu identificar as restricoes");
		}
		return restricoes;
	}
	
	/*public static boolean verificarSeValorSegueRestricoes(String valor, Campo campo, ArrayList<String> colunas)
	{
		boolean aprovado = false;
		
		String nomeCampo = campo.getNome();
		int indiceColuna = Tabela.INDICE_COLUNA_NAO_ENCONTRADA;
		for(int j=0; j < colunas.size(); j++)
		{
			if(colunas.get(j).equalsIgnoreCase(nomeCampo))
				indiceColuna = j;
		}
		
		if(indiceColuna == Tabela.INDICE_COLUNA_NAO_ENCONTRADA)
		{
			String valorNulo = Restricoes.getValorSeValorEntradaNulo(campo);
			if(!valor.equals(Restricoes.VALOR_ERRO))
				novaLinha.addColuna(valorNulo);
			else
				gerouErro = true;
		}
		else
		{
			if(Restricoes.verificarSeValorSegueRegrasSigned(conteudoRegistro.get(indiceColuna), campo))
			{
				if(campo.getRestricoes().isPrimary() || campo.getRestricoes().isUnique())
				{
					if(Restricoes.verificarSeValorIsUnico(converterPraColuna(conteudoRegistro.get(indiceColuna), campo), registros, indiceCampo))
					{
						novaLinha.addColuna(converterPraColuna(conteudoRegistro.get(indiceColuna), campo));
					}
					else
					{
						gerouErro = true;
						Erro.gerarErro("O valor("+conteudoRegistro.get(indiceColuna)+") do campo "+campo.getNome()+" deve ser unico!");
					}
				}
				else
				{
					novaLinha.addColuna(converterPraColuna(conteudoRegistro.get(indiceColuna), campo));
				}
					
			}
		
		return aprovado;
	}*/
	
	/**
	 * Se o valor do registro for nulo, ele verificar� as restri��es e retornar� o valor convertido ou gerar� um erro.
	 * @param campo Campo que ser� analisado
	 * @return Retorna o valor nulo para aquele tipo de dado. Se retornar -1, significa que aconteceu algum erro.
	 */
	public static String getValorSeValorEntradaNulo(Campo campo)
	{
		String valorConvertido = VALOR_ERRO;
		// Nao tem a coluna, adiciona valor default ou gera um erro se nao estiver como NOT NULL
		if(campo.getRestricoes().isPrimary())
		{
			// adiciona o contador do AI.
			valorConvertido = converterValorParaFormatoTipo(""+campo.getRestricoes().getContador(), campo);
			//novaLinha.addColuna(converterPraColuna(""+campo.getRestricoes().getContador(), campo));
			campo.getRestricoes().aumentarContador();
		}
		else if(campo.getRestricoes().isNulo())
		{
			if(campo.getRestricoes().contemDefaultValue())
				valorConvertido = converterValorParaFormatoTipo(campo.getRestricoes().getDefaultValue(), campo);
			else
				valorConvertido = converterValorParaFormatoTipo(VALOR_NULL, campo);					
		}
		else if(!campo.getRestricoes().isNulo())
		{
			// gerar erro
			Erro.gerarErro("O campo " + campo.getNome() + " nao aceita valor nulo!");
		}
		
		return valorConvertido;
	}
	
	public static boolean verificarSeValorIsUnico(String valor, ArrayList<Registro> registros, int indiceCampo)
	{
		boolean valorUnico = true;
		//System.out.println("Verificar se valor e unico. Qts de registros : " + registros.size());
		for(Registro registro : registros)
		{
			//System.out.println(registro.getConteudos().get(indiceCampo) + "== " + valor + "?");
			if(registro.getConteudos().get(indiceCampo).equalsIgnoreCase(valor))
				valorUnico = false;
		}
		return valorUnico;
	}
	
	private static String converterValorParaFormatoTipo(String valorCampo, Campo campo)
	{
		if(valorCampo.equals(VALOR_NULL))
		{
			return campo.getTipo().converter();
		}
		else
		{	
			return campo.getTipo().converter(valorCampo);
		}	
	}
	

	public static boolean verificarSeValorSegueRegrasSigned(String valor, Campo campo)
	{
		if(campo.getRestricoes().isSigned() && (campo.getTipo() instanceof TipoInt || campo.getTipo() instanceof TipoFloat))
		{
			// Aceita numeros negativos
			//novaLinha.addColuna(converterPraColuna(conteudoRegistro.get(indiceColuna), campo));
			return true;
		}
		else if(campo.getTipo() instanceof TipoInt || campo.getTipo() instanceof TipoFloat)
		{
			// Nao aceita numeros negativos
			if(valor.charAt(0) == '-')
			{
				Erro.gerarErro("O campo " + campo.getNome() + " nao aceita valor negativo("+valor+").");
				return false;
			}
			else
			{
				//novaLinha.addColuna(converterPraColuna(conteudoRegistro.get(indiceColuna), campo));
				return true;
			}
		}
		return true;
	}

	public String toString()
	{
		String s = "";
		s += "isPrimary=" + primary + ";";
		s += "isAutoincrement=" + autoIncrement + ";";
		if(autoIncrement)
			s += "ai-value=" + contador + ";"; 
		s += "isSigned=" + signed + ";";
		s += "isNulo=" + nulo + ";";
		s += "isUnique=" + unique + ";";
		s += "defaultValue=" + defaultValue;
		
		return s;
	}
	
	public boolean contemDefaultValue()
	{
		return (!defaultValue.equals(null));
	}


	public boolean isPrimary() {
		return primary;
	}

	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	public boolean isAutoIncrement() {
		return autoIncrement;
	}

	public void setAutoIncrement(boolean autoIncrement) {
		this.autoIncrement = autoIncrement;
	}

	public boolean isSigned() {
		return signed;
	}

	public void setSigned(boolean signed) {
		this.signed = signed;
	}

	public boolean isNulo() {
		return nulo;
	}

	public void setNulo(boolean nulo) {
		this.nulo = nulo;
	}

	public boolean isUnique() {
		return unique;
	}

	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public int getContador() {
		return contador;
	}

	public void setContador(int contador) {
		this.contador = contador;
	}
	
	
	
}
