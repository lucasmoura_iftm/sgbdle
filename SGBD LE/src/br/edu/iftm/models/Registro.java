package br.edu.iftm.models;

import java.io.File;
import java.util.ArrayList;

import br.edu.iftm.models.tipos.Tipo;

public class Registro {
	private ArrayList<String> conteudos;
	
	public Registro()
	{
		conteudos = new ArrayList<String>();
	}
	
	public void addColuna(String valor)
	{
		conteudos.add(valor);
	}
	
	public static ArrayList<Registro> carregarRegistros(File arquivoRegistros, ArrayList<Campo> campos)
	{
		ArrayList<Registro> registros = new ArrayList<Registro>();
		System.out.println("Arquivo registro: " + arquivoRegistros.getName());
		String conteudoArquivo = Arquivo.lerArquivo(arquivoRegistros);
		
		if(conteudoArquivo.isEmpty())
			return registros;
		
		int indexRegistro = 0;
		int contadorCampos = 0;
		boolean sair = false;

		System.out.println("Qtd campos : " + campos.size());

		Registro novoRegistro = new Registro();
		while(!sair)
		{
			System.out.println("Contador campos = " + contadorCampos);
			String[] resultado = campos.get(contadorCampos).getTipo().obterValor(conteudoArquivo);
			conteudoArquivo = resultado[Tipo.INDICE_LINHA];
			System.out.println("Valor "+contadorCampos+": " + resultado[Tipo.INDICE_VALOR]);
			
			novoRegistro.addColuna(resultado[Tipo.INDICE_VALOR]);
			
			contadorCampos++;
			if(contadorCampos == campos.size())
			{
				indexRegistro++;
				registros.add(novoRegistro);
				novoRegistro = new Registro();
				contadorCampos = 0;
				
				if(conteudoArquivo.replace(System.lineSeparator(), "").isEmpty())
					sair = true;
			}
			
		}
		
		System.out.println("Quantidade de registros: " + registros.size());
		for(int i=0; i < registros.size(); i++)
			System.out.println(registros.get(i));
		
		return registros;
	}
	

	public ArrayList<String> getConteudos() {
		return conteudos;
	}

	public void setConteudos(ArrayList<String> conteudos) {
		this.conteudos = conteudos;
	}
	
	public String toString()
	{
		String s = "";
		for(int i=0; i < conteudos.size(); i++)
		{
			s += conteudos.get(i);
			if(i != conteudos.size()-1)
				s += ";";
		}
		
		return s;
	}
}
