package br.edu.iftm.controllers;

import java.util.ArrayList;

import br.edu.iftm.models.Banco;
import br.edu.iftm.models.Campo;
import br.edu.iftm.models.ConteudoConsulta;
import br.edu.iftm.models.Tabela;
import br.edu.iftm.views.CampoTabela;
import javafx.scene.control.TreeItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Controlador {
	private ArrayList<Banco> bancos;
	
	public Controlador()
	{
		carregarBancos();
	}
	
	public int getQtdRegistros(int idBanco, int idTabela)
	{
		return bancos.get(idBanco).getTabelas().get(idTabela).getRegistros().size();
	}
	
	public String[] getVetorCampos(int idBanco, int idTabela)
	{
		int qtdCampos = bancos.get(idBanco).getTabelas().get(idTabela).getCampos().size();
		String[] vetorCampos = new String[qtdCampos];
		
		for(int i=0; i < qtdCampos; i++)
		{
			vetorCampos[i] = bancos.get(idBanco).getTabelas().get(idTabela).getCampos().get(i).getNome();
		}
		
		return vetorCampos;
	}
	
	public String[][] getMatrizRegistros(int idBanco, int idTabela)
	{
		int qtdRegistros = bancos.get(idBanco).getTabelas().get(idTabela).getRegistros().size();
		int qtdCampos = bancos.get(idBanco).getTabelas().get(idTabela).getCampos().size();
		String[][] dadosMatriz = new String[qtdRegistros][qtdCampos];
		for(int i=0; i < qtdRegistros; i++)
		{
			ArrayList<String> conteudos = bancos.get(idBanco).getTabelas().get(idTabela).getRegistros().get(i).getConteudos();
			for(int j=0; j < qtdCampos; j++)
			{
				dadosMatriz[i][j] = conteudos.get(j);
			}
		}
		
		return dadosMatriz;
	}
	
	public int buscarIdBanco(String nomeBanco)
	{
		return Banco.getIdBanco(bancos, nomeBanco);
	}
	
	public int buscarIdTabela(int idBanco, String nomeTabela)
	{
		return Tabela.getIdTabela(bancos.get(idBanco).getTabelas(), nomeTabela);
	}
	
	private void carregarBancos()
	{
		bancos = Banco.carregarBancos();
	}
	
	public ConteudoConsulta executarQuery(String query, String nomeBanco)
	{
		return SqlParser.executar(query.toUpperCase(), this, nomeBanco);
	}
	
	public TreeItem<String> getConteudoTreeView()
	{
		Image imgBanco = new Image("file:imagens//db.png", 20, 20, true, false);
		Image imgTable = new Image("file:imagens//table.jpg", 20, 20, true, false);
		Image imgTableNew = new Image("file:imagens//tablenew.png", 20, 20, true, false);
		TreeItem<String> itemBds = new TreeItem<String>("Banco de dados");
		itemBds.setExpanded(true);
		for(Banco banco : bancos)
		{
			TreeItem<String> itemBd = new TreeItem<String> (banco.getNome(), new ImageView(imgBanco));
			itemBd.setExpanded(true);
			TreeItem<String> itemTbNew = new TreeItem<String> ("New", new ImageView(imgTableNew));
			itemBd.getChildren().add(itemTbNew);
			for(Tabela tabela : banco.getTabelas())
			{
				TreeItem<String> itemTb = new TreeItem<String> (tabela.getNome(), new ImageView(imgTable));
				itemBd.getChildren().add(itemTb);
			}
			itemBds.getChildren().add(itemBd);
			
		}
		return itemBds;
	}

	public ArrayList<Banco> getBancos() {
		return bancos;
	}

	public void setBancos(ArrayList<Banco> bancos) {
		this.bancos = bancos;
	}
	
	
	
}
