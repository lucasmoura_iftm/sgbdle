package br.edu.iftm.controllers;

import java.math.MathContext;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import br.edu.iftm.models.Banco;
import br.edu.iftm.models.Campo;
import br.edu.iftm.models.ConteudoConsulta;
import br.edu.iftm.models.Erro;
import br.edu.iftm.models.Registro;
import br.edu.iftm.models.Tabela;
import br.edu.iftm.models.tipos.TipoChar;
import br.edu.iftm.models.tipos.TipoText;
import br.edu.iftm.models.tipos.TipoVarChar;

public class SqlParser {
	public static final String SELECT = "SELECT";
	public static final String INSERT = "INSERT";
	public static final String UPDATE = "UPDATE";
	public static final String DELETE = "DELETE";
	
	public static ConteudoConsulta executar(String query, Controlador controlador, String nomeBanco)
	{
		ConteudoConsulta cc = new ConteudoConsulta(0, 0);
		Pattern padrao = Pattern.compile("(\\w+) ");
		Matcher matcher = padrao.matcher(query);
		if(matcher.find())
		{
			String operacao = matcher.group(1).toUpperCase();
			
			switch (operacao) {
			case SELECT:
				return executarSelect(query, controlador, nomeBanco);
				//break;
			case INSERT:
				executarInsert(query);	
				break;
			case UPDATE:
				executarUpdate(query);
			case DELETE:
				executarDelete(query);
			}
		}
		else
		{
			Erro.gerarErro("O tipo de operacao nao foi identificado!");
		}
		
		return cc;
	}
	
	private static ArrayList<String> getCampos(String stringCampos)
	{
		ArrayList<String> campos = new ArrayList<String>();
		Pattern padrao = Pattern.compile("*");
		//Matcher matcher = padrao.matcher(query);
		return campos;
	}
	
	private static boolean validarWhereSemParenteses(String conteudo)
	{
		boolean aprovado = true;
		//ID =1 AND C = 1'
		String[] conteudoDividido = conteudo.split(" ");
		
		return aprovado;
	}
	
	private static ConteudoConsulta executarSelect(String query, Controlador controlador, String nomeBanco)
	{
		
		//Pattern padrao = Pattern.compile("SELECT\\s+(\\*|[\\w-]+)\\s+FROM\\s+([\\w-]+)");
		//Pattern padrao = Pattern.compile("SELECT\\s+(\\*|(\\w+(,\\s+[\\w]+)+)?)\\s+FROM\\s+(\\w+)");
		//query = query.trim();
		Pattern padrao = Pattern.compile("SELECT (\\*|(\\w+(,\\s+[\\w]+)*))\\s+FROM\\s+(\\w+)(\\s+WHERE\\s+([\\w\\W]+))?");
		//Pattern padrao = Pattern.compile("");
		Matcher matcher = padrao.matcher(query);
		System.out.println("aki");
		if(matcher.find())
		{
			System.out.println("Campo: " + matcher.group(1));
			System.out.println("Tabela: " + matcher.group(4));
			System.out.println("Where: " + matcher.group(6));
			String nomeTabela = matcher.group(4);
			String conteudoWhere = matcher.group(6);
			
			if(conteudoWhere == null)
				conteudoWhere = "";
			
			conteudoWhere = conteudoWhere.trim();
			String conteudoWhereSemParenteses = conteudoWhere./*replace(")", " ").replace("(", " ").*/trim();
			
			
			conteudoWhereSemParenteses = conteudoWhereSemParenteses.replaceAll("\\s+", " ");
			
			System.out.println("Where sem parenteses: '" + conteudoWhereSemParenteses+"'");
			//seAtendeAsCondicoes(controlador.getBancos().get(0).getTabelas().get(0).getCampos().get(0), conteudoWhere);
			
			
			/*Pattern padraoWhere = Pattern.compile("(NOT\\s+)?([\\w])+\\s*(=|>|<|>=|<=|<>)\\s*([\\w]+|'[^'\"]+'|\"[^\"]+\")(\\s*(AND|OR)\\s+((NOT\\s+)?([\\w])+\\s*(=|>|<|>=|<=|<>)\\s*([\\w]+|'[^'\"]+'|\"[^\"]+\")))*");
			Matcher matcherWhere = padraoWhere.matcher(conteudoWhereSemParenteses);
			if(matcherWhere.find())
			{
				System.out.println("Aprovado! Qtd de grupos: " + matcherWhere.groupCount());
			}
			else
			{
				System.out.println("Reprovado! Qtd de grupos: " + matcherWhere.groupCount());
			}*/
			
			//return null;
			return gerarResultado(nomeTabela, controlador, nomeBanco, conteudoWhere);
		}
		else
		{
			Erro.gerarErro("O select esta incorreto!");
		}
		return null;
	}
	
	private static int getIdCampos(ArrayList<String> campos, String nomeCampo)
	{
		for(int i=0; i  < campos.size(); i++)
		{
			if(campos.get(i).equalsIgnoreCase(nomeCampo))
				return i;
		}
		return -1;
	}
	
	private static boolean seValorECampo(String valor)
	{
		Pattern padrao = Pattern.compile("([\\d\\.]+|'[^'\"]+'|\"[^\"]+\")");
		Matcher matcher = padrao.matcher(valor);
		return !matcher.find();
	}
	
	private static String substituirCampoPeloValor(String valor, String condicoes, int idCampo, Registro registro, Tabela tabela)
	{
		String nomeCampo = tabela.getCampos().get(idCampo).getNome();
		valor = registro.getConteudos().get(idCampo); // Substitui pelo valor do campo
		if(tabela.getCampos().get(idCampo).getTipo() instanceof TipoText || tabela.getCampos().get(idCampo).getTipo() instanceof TipoVarChar
		   || tabela.getCampos().get(idCampo).getTipo() instanceof TipoChar)
		{
			valor = "'" + valor + "'";
		}
		
		valor = condicoes.toLowerCase().replace(nomeCampo, valor);
		return valor;
	}
	
	private static boolean seAtendeAsCondicoes(ArrayList<String> campos, Registro registro,  String condicoes, Tabela tabela)
	{
		boolean aprovado = true;
		boolean sair = false;
		Pattern padrao = Pattern.compile("((\\w+)\\s*(=|>|<|>=|<=|<>)\\s*([\\w]+|'[^'\"]+'|\"[^\"]+\"))");
		Matcher matcher = padrao.matcher(condicoes);
		System.out.println("Testando: '" +condicoes+"'");
		while(!sair)
		{
			if(matcher.find())
			{
				System.out.println("Grupos: " + matcher.groupCount());
				// Encontrou alguma condição
					String condicao = matcher.group(0);
					System.out.println("Condicao encontrada:" + condicao);
					System.out.println("Campo: " + matcher.group(2) +"; op:"+matcher.group(3)+"; valor="+matcher.group(4));
					String campo = matcher.group(2);
					String op = matcher.group(3);
					String valor = matcher.group(4)/*.replace("'", "").replace("\"", "")*/;
					int idCampo = getIdCampos(campos, campo);
					condicoes = substituirCampoPeloValor(valor, condicoes, idCampo, registro, tabela); // Substitui o campo (esquerda)
					if(seValorECampo(valor))
					{
						int idCampo2 = getIdCampos(campos, valor);
						condicoes = substituirCampoPeloValor(valor, condicoes, idCampo2, registro, tabela);
						/*String nomeCampoAntigo = valor;
						valor = registro.getConteudos().get(idCampo2); // Substitui pelo valor do campo
						if(tabela.getCampos().get(idCampo2).getTipo() instanceof TipoText || tabela.getCampos().get(idCampo2).getTipo() instanceof TipoVarChar
						   || tabela.getCampos().get(idCampo2).getTipo() instanceof TipoChar)
						{
							valor = "'" + valor + "'";
						}
						
						condicoes = condicoes.replace(nomeCampoAntigo, valor);*/
					}
					else
					{
						
					}
					
					/*valor = valor.replace("'", "").replace("\"", "");
					boolean resultado = false;
					switch(op)
					{
						case "=":
							resultado = registro.getConteudos().get(idCampo).equalsIgnoreCase(valor);
							break;
						case ">":
							//
							resultado = Float.parseFloat(registro.getConteudos().get(idCampo)) > Float.parseFloat(valor);
							break;
						case "<":
							//
							resultado = Float.parseFloat(registro.getConteudos().get(idCampo)) < Float.parseFloat(valor);
							break;
						case ">=":
							//
							resultado = Float.parseFloat(registro.getConteudos().get(idCampo)) >= Float.parseFloat(valor);
							break;
						case "<=":
							//
							resultado = Float.parseFloat(registro.getConteudos().get(idCampo)) <= Float.parseFloat(valor);
							break;
						case "<>":
							//
							resultado = !registro.getConteudos().get(idCampo).equalsIgnoreCase(valor);
							break;
					}
					
					condicoes = condicoes.replace(condicao, ""+resultado);*/
			}
			else
			{
				// Não encontrou nenhuma condição (acabou ou não tem)}
				System.out.println("Condicao nao encontrada");
				sair = true;
			}
		}
		/*
		Sobrou apenas os boolean, os NOT e os parenteses, o objetivo � deixar somente os boolean.
		Para remover uma dupla de parenteses, � preciso deixar somente um boolean l� dentro
		*/
		sair = false;
		condicoes = condicoes.replaceAll("\\s+", " "); // Remove os excesso de espa�os
		condicoes = condicoes.toLowerCase().replace("and", "&&").replace("or", "||").replace("not", "!").replace("\"", "'").replace("=", "==").replace("<>", "!=");
		System.out.println("Condicoes no formato javascript: '" + condicoes+"'");
		ScriptEngineManager sem = new ScriptEngineManager();
		ScriptEngine se = sem.getEngineByName("JavaScript");
		String resultado = "Erro na expressao booleana";
		try {
			resultado = se.eval(condicoes).toString();
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*while(!sair)
		{
			
		}*/
		//padrao = Pattern.compile("");
		//matcher = padrao.matcher(condicoes);
		
		//condicoes = condicoes.replace("(", " ").replace("(", " ").trim();
		//condicoes = condicoes.replaceAll("\\s+", " ");
		// remove os parenteses
		// verifica os or e and
		
		System.out.println("String condicoes no final: " + resultado);
		// (true) or false and true or (true)
		
		return Boolean.parseBoolean(resultado);
	}
	
	private static ConteudoConsulta gerarResultado(String nomeTabela, Controlador controlador, String nomeBanco, String condicoes)
	{
		int idBanco = Banco.getIdBanco(controlador.getBancos(), nomeBanco);
		int idTabela = Tabela.getIdTabela(controlador.getBancos().get(idBanco).getTabelas(), nomeTabela);
		Tabela tabela = controlador.getBancos().get(idBanco).getTabelas().get(idTabela);
		ArrayList<String> campos = new ArrayList<String>();
		
		for(int i=0; i < tabela.getCampos().size(); i++)
		{
			campos.add(tabela.getCampos().get(i).getNome());
		}
		
		return gerarResultado(campos, nomeTabela, controlador, nomeBanco, condicoes, tabela);
	}
	
	private static ConteudoConsulta gerarResultado(ArrayList<String> campos, String nomeTabela, Controlador controlador, String nomeBanco, String condicoes, Tabela tabela)
	{
		int idBanco = Banco.getIdBanco(controlador.getBancos(), nomeBanco);
		int idTabela = Tabela.getIdTabela(controlador.getBancos().get(idBanco).getTabelas(), nomeTabela);
		ArrayList<Registro> registros = controlador.getBancos().get(idBanco).getTabelas().get(idTabela).getRegistros();
		// Se tiver condi��es, vou removendo desse vetor
		ConteudoConsulta cc = new ConteudoConsulta(campos.size(), registros.size());
		
		for(String campo : campos)
		{
			cc.addColuna(campo);
		}
		
		for(Registro registro : registros)
		{
			if(seAtendeAsCondicoes(campos, registro, condicoes, tabela))
			{
				String[] registroValores = new String[campos.size()];
				for(int i=0; i < campos.size(); i++)
				{
					registroValores[i] = registro.getConteudos().get(i);
				}
				cc.addValor(registroValores);
			}
		}
		
		
		return cc;
	}
	
	private static void executarInsert(String query)
	{
		
	}
	
	private static void executarUpdate(String query)
	{
		
	}
	
	private static void executarDelete(String query)
	{
		
	}
}
