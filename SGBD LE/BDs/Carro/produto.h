'id' int(6) = {isPrimary=true;isAutoincrement=true;ai-value=0;isSigned=true;isNulo=false;isUnique=false;defaultValue=null}
'nome' varchar(10) = {isPrimary=false;isAutoincrement=false;isSigned=false;isNulo=false;isUnique=false;defaultValue=null}
'descricao' text(0) = {isPrimary=false;isAutoincrement=false;isSigned=false;isNulo=false;isUnique=false;defaultValue=null}
'etiqueta' char(30) = {isPrimary=false;isAutoincrement=false;isSigned=false;isNulo=false;isUnique=false;defaultValue=null}
'validade' date(10) = {isPrimary=false;isAutoincrement=false;isSigned=false;isNulo=false;isUnique=false;defaultValue=null}
'fabricacao' datetime(19) = {isPrimary=false;isAutoincrement=false;isSigned=false;isNulo=false;isUnique=false;defaultValue=null}
'preco-sugerido' float(6) = {isPrimary=false;isAutoincrement=false;isSigned=false;isNulo=true;isUnique=true;defaultValue=0.05}
